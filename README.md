# 프로젝트명 : 여행팅
***

## 개발 기간
___2023-12-04 ~ 2024-02-24___
***

## 프로젝트 목적

## 기존에 있던 랜덤 미팅 어플에서 여행을 추가하여 차별화 하였습니다

## 팀원구성
***
## 김남호(web), 배병수(back-end), 주명돈(APP)

## **개발 환경**
***

### back-end
 <img src="https://img.shields.io/badge/swagger-85EA2D?style=flat&logo=swagger&logoColor=white" />
 <img src="https://img.shields.io/badge/springboot-69D3A7?style=flat&logo=springboot&logoColor=white" />
<img src="https://img.shields.io/badge/docker-2496ED?style=flat&logo=docker&logoColor=white" />



### front(web)
<img src="https://img.shields.io/badge/html5-E34F26?style=flat&logo=html5&logoColor=white" />
<img src="https://img.shields.io/badge/css3-1572B6?style=flat&logo=css3&logoColor=white" />
<img src="https://img.shields.io/badge/javascript-F7DF1E?style=flat&logo=javascript&logoColor=white" />
<img src="https://img.shields.io/badge/nuxtdotjs-00DC82?style=flat&logo=nuxtdotjs&logoColor=white" />
<img src="https://img.shields.io/badge/nodedotjs-5FA04E?style=flat&logo=nodedotjs&logoColor=white" />
<img src="https://img.shields.io/badge/npm-CB3837?style=flat&logo=npm&logoColor=white" />

### front(APP)
<img src="https://img.shields.io/badge/flutter-02569B?style=flat&logo=flutter&logoColor=white" />
<img src="https://img.shields.io/badge/dart-0175C2?style=flat&logo=dart&logoColor=white" />


### DB
<img src="https://img.shields.io/badge/postgresql-4169E1?style=flat&logo=postgresql&logoColor=white" />

## 채택한 개발 기술
***
## front(web)
### elementUI 

   + 다른 ui프로그램에 비해서 view와 하기가 수월하며 특히 vuejs와의 연동이 용이합니다.  
   + 비교적 템플릿의 양이 많아 개발 시간을 단축시킬 수 있었습니다.

### nuxtJS 
   + 초기 프로젝트 설정 비용 감소와 생산성 향상
     +  라우터,스토어 등의 라이브러리 설치 및 설정 파일이 따로 필요하지 않아 프로젝트를 시작했을떄 시간 비용을 감축할수 있었습니다
   + 페이지 로딩 속도 향상
     + 브라우저가 하는 일을 나누어서 주기 때문에 실행시 시간을 단축 시킬수 있었습니다. 

## front(APP)

### dio
   +  dio 라이브러리는 Json 파일이 디코딩된 상태로 리턴되기에 좀 더 사용하기에 용이합니다.
   + options, Interceptor 통해 다양한 기능을 한 번에 핸들링 할 수 있다는 장점이 있어 개발을 더욱 효율적으로 할 수 있었습니다.

## back-end

### IntelliJ
   + 이클립스에 비해 IDE의 안정성이 뛰어나 프로그램이 가볍고 플러그인 설치시 충돌이 일어나 확률도 적습니다.
   + java 개발은 보통 준비하는 시간이 상당부 차지하지만 IntelliJ 편하게 VS처럼 단계 별로 설정후 프로젝트를 거의 바로 시작해도 될만큼 초기 준비시간이 단축됩니다.
### docker(DB)
   + 컨테이너화된 앱의 인스턴스는 가상 머신에 비해 사용하는 메모리가 훨씬 적고 가동과 중단이 빠르며 호스트 하드웨어 상의 밀집도가 높아 이는 IT 지출 감소로 이어지는 장점이 있습니다 .
   
