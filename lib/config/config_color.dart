import 'package:flutter/material.dart';

const Color colorPrimary = Color.fromRGBO(0, 0, 0, 1);
const Color colorSecondary = Color.fromRGBO(241, 241, 241, 1.0);
const Color colorThird = Color.fromRGBO(150, 150, 150, 1.0);
const Color colorRed = Color.fromARGB(255, 216, 34, 34);
const Color colorGray = Color.fromRGBO(0, 0, 0, 0.35);
const Color colorDarkGray = Color.fromRGBO(0, 0, 0, 0.65);
const Color colorLightGray = Color.fromRGBO(0, 0, 0, 0.1);
