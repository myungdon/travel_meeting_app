import 'dart:ui';

const double fontSizeSuper = 24;
const double fontSizeBig = 18;
const double fontSizeMid = 16;
const double fontSizeSmall = 14;
const double fontSizeMicro = 12;

const FontWeight fontWeightSuper = FontWeight.w900;
const FontWeight fontWeightBig = FontWeight.w700;
const FontWeight fontWeightMid = FontWeight.w500;
const FontWeight fontWeightSmall = FontWeight.w300;
const FontWeight fontWeightMicro = FontWeight.w100;

const double appbarSizeHeight = 50;