// 1:1문의 단수용 작은 그릇

class QuestionResponse {
  String memberName;
  String title;
  String content;
  String inputDay;

  QuestionResponse(this.memberName, this.title, this.content, this.inputDay);

  factory QuestionResponse.fromJson(Map<String, dynamic> json) {
    return QuestionResponse(
      json['memberName'],
      json['title'],
      json['content'],
      json['inputDay'],
    );
  }
}