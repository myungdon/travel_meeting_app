// 댓글 복수용 작은 틀

class CommentsItem {
  num id;
  String comments;
  String writingTime;

  CommentsItem(this.id, this.comments, this.writingTime);

  factory CommentsItem.fromJson(Map<String, dynamic> json) {
    return CommentsItem(
      json['id'],
      json['comments'],
      json['writingTime'],
    );
  }
}