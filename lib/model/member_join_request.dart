// 회원 가입 틀

// class MemberJoinRequest {
//   MemberJoinRequest({
//     required this.data,
//   });
//   Data data;
//
//   factory MemberJoinRequest.fromJson(Map<String, dynamic> json) => MemberJoinRequest(
//     data: Data.fromJson(json["data"]),
//   );
//
//   Map<String, dynamic> toJson(data) => {
//     "data": data.toJson(),
//   };
// }
//
// class Data {
//   Data({
//   required this.name,
//   required this.account,
//   required this.password,
//   required this.passwordRe,
//   required this.birthday,
//   required this.phoneNumber,
//   required this.isMan,
//   required this.eMail,
//   required this.address
// });
//
//   String name;
//   String account;
//   String password;
//   String passwordRe;
//   String birthday;
//   String phoneNumber;
//   String? isMan;
//   String eMail;
//   String address;
//
//   factory Data.fromJson(Map<String, dynamic> json) => Data(
//       name: '',
//       account: '',
//       password: '',
//       passwordRe: '',
//       birthday: '',
//       phoneNumber: '',
//       isMan: '',
//       eMail: '',
//       address: '',
//   );
//
//   Map<String, dynamic> toJson() => {
//     "name": name,
//     "account": account,
//     "password": password,
//     "passwordRe": passwordRe,
//     "birthday": birthday,
//     "phoneNumber": phoneNumber,
//     "isMan": isMan,
//     "eMail": eMail,
//     "address": address,
//   };
// }

// 2차 시도
// 3차 시도

class MemberJoinRequest {
  String name;
  String account;
  String password;
  String passwordRe;
  String birthday;
  String phoneNumber;
  String isMan;
  String eMail;
  String address;

  MemberJoinRequest(
    this.name,
    this.account,
    this.password,
    this.passwordRe,
    this.birthday,
    this.phoneNumber,
    this.isMan,
    this.eMail,
    this.address
);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data["name"] = this.name;
    data["account"] = this.account;
    data["password"] = this.password;
    data["passwordRe"] = this.passwordRe;
    data["birthday"] = this.birthday;
    data["phoneNumber"] = this.phoneNumber;
    data["isMan"] = this.isMan;
    data["eMail"] = this.eMail;
    data["address"] = this.address;
    return data;
  }
}