// 멤버 단수용 큰틀

import 'package:travel_meeting_app/model/member_response.dart';

class MemberDetailResult {
  String msg;
  num code;
  MemberResponse data;

  MemberDetailResult(this.msg, this.code, this.data);

  factory MemberDetailResult.fromJson(Map<String, dynamic> json) {
    return MemberDetailResult(
        json['msg'],
        json['code'],
        MemberResponse.fromJson(json['data'])
    );
  }
}