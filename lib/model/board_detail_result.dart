// 게시판 단수용 큰틀

import 'package:travel_meeting_app/model/board_response.dart';

class BoardDetailResult{
  String msg;
  num code;
  BoardResponse data;

  BoardDetailResult(this.msg, this.code, this.data);

  factory BoardDetailResult.fromJson(Map<String, dynamic> json){
    return BoardDetailResult(
      json['msg'],
      json['code'],
      BoardResponse.fromJson(json['data'])
    );
  }
}