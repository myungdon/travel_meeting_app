// 매칭 복수용 작은 틀

class MatchingItem {
  num id;
  String matchingName;
  String matchingImg;
  String matchingCode;
  num matchingPrice;
  String matchingCategory;
  String dateStart;
  String dateEnd;
  String address;
  String pension;
  String etcMemo;

  MatchingItem(
      this.id,
      this.matchingName,
      this.matchingImg,
      this.matchingCode,
      this.matchingPrice,
      this.matchingCategory,
      this.dateStart,
      this.dateEnd,
      this.address,
      this.pension,
      this.etcMemo,
      );

  factory MatchingItem.fromJson(Map<String, dynamic> json) {
    return MatchingItem(
      json['id'],
      json['matchingName'],
      json['matchingImg'],
      json['matchingCode'],
      json['matchingPrice'],
      json['matchingCategory'],
      json['dateStart'],
      json['dateEnd'],
      json['address'],
      json['pension'],
      json['etcMemo'],
    );
  }
}

