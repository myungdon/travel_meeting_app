class LoginResponse {
  String token;
  String name;
  LoginResponse(this.token, this.name);
  factory LoginResponse.fromJson(Map<String, dynamic> json) {
    return LoginResponse(
      json['memberId'].toString(),
      json['memberName']
    );
  }
}