// 게시판 단수용 작은 그릇

class BoardResponse {
  String img;
  String memberName;
  String dateWrite;
  String title;
  String content;

  BoardResponse(this.img, this.memberName, this.dateWrite, this.title, this.content);

  factory BoardResponse.fromJson(Map<String, dynamic> json) {
    return BoardResponse(
      json['img'],
      json['memberName'],
      json['dateWrite'],
      json['title'],
      json['content'],
    );
  }
}