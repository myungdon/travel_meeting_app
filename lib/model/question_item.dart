// 1:1문의 복수 작은 틀

class QuestionItem {
  num id;
  String memberName;
  String title;
  String content;
  String inputDay;

  QuestionItem(this.id, this.memberName, this.title, this.content, this.inputDay);

  factory QuestionItem.fromJson(Map<String, dynamic> json){
    return QuestionItem(
      json['id'],
      json['memberName'],
      json['title'],
      json['content'],
      json['inputDay'],
    );
  }
}