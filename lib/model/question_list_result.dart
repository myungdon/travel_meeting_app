// 1:1문의 복수용 큰틀

import 'package:travel_meeting_app/model/question_item.dart';

class QuestionListResult {
  String msg;
  num code;
  List<QuestionItem> list;
  num totalCount;
  num totalPage;
  num currentPage;

  QuestionListResult(this.msg, this.code, this.list, this.totalCount, this.totalPage, this.currentPage);

  factory QuestionListResult.fromJson(Map<String, dynamic> json){
    return QuestionListResult(
      json['msg'],
      json['code'],
      json['list'] != null
          ? (json['list'] as List).map((e) => QuestionItem.fromJson(e)).toList()
          : [],
      json['totalCount'],
      json['totalPage'],
      json['currentPage'],
    );
  }
}