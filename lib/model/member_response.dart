// 멤버 단수용 작은 틀

class MemberResponse {
 // num id;
 String name;
 bool? isNormal;
 // String account;
 // String password;
 // String birthday;
 // String phoneNumber;
 // String dateJoin;
 // String isMan;
 // String eMail;
 // String address;
 String grade;
 // String reason;
 // String state;
 // String etcMemo;

 MemberResponse(
     // this.id,
     this.name,
     this.isNormal,
     // this.account,
     // this.password,
     // this.birthday,
     // this.phoneNumber,
     // this.dateJoin,
     // this.isMan,
     // this.eMail,
     // this.address,
     this.grade,
     // this.reason,
     // this.state,
     // this.etcMemo,
     );

 factory MemberResponse.fromJson(Map<String, dynamic> json) {
  return MemberResponse(
   // json['id'],
   json['name'],
   json['isNormal'],
   // json['account'],
   // json['password'],
   // json['birthday'],
   // json['phoneNumber'],
   // json['dateJoin'],
   // json['isMan'],
   // json['eMail'],
   // json['address'],
   json['grade'],
   // json['reason'],
   // json['state'],
   // json['etcMemo'],
  );
 }
}