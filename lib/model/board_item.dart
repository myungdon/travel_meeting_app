// 게시판 복수 작은 틀

class BoardItem {
  num id;
  String img;
  String memberName;
  String dateWrite;
  String title;
  String content;

  BoardItem(this.id, this.img, this.memberName, this.dateWrite, this.title, this.content);

  factory BoardItem.fromJson(Map<String, dynamic>json) {
    return BoardItem(
      json['id'],
      json['img'],
      json['memberName'],
      json['dateWrite'],
      json['title'],
      json['content'],
    );
  }
}