// 댓글 복수용 큰틀

import 'package:travel_meeting_app/model/comments_item.dart';

class CommentsListResult {
  String msg;
  num code;
  List<CommentsItem> list;
  num totalCount;
  num totalPage;
  num currentPage;

  CommentsListResult(this.msg, this.code, this.list, this.totalCount, this.totalPage, this.currentPage);

  factory CommentsListResult.fromJson(Map<String, dynamic> json) {
    return CommentsListResult(
      json['msg'],
      json['code'],
      json['list'] != null ?
      (json['list'] as List).map((e) => CommentsItem.fromJson(e)).toList()
          : [],
      json['totalCount'],
      json['totalPage'],
      json['currentPage'],
    );
  }
}