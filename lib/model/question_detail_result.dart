// 1:1문의 단수용 큰틀

import 'package:travel_meeting_app/model/question_response.dart';

class QuestionDetailResult{
  String msg;
  num code;
  QuestionResponse data;

  QuestionDetailResult(this.msg, this.code, this.data);

  factory QuestionDetailResult.fromJson(Map<String, dynamic> json){
   return QuestionDetailResult(
     json['msg'],
     json['code'],
     QuestionResponse.fromJson(json['data']),
   );
  }
}