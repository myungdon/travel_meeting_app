// 매칭 단수용 큰틀

import 'package:travel_meeting_app/model/matching_response.dart';

class MatchingDetailResult {
  String msg;
  num code;
  MatchingResponse data;

  MatchingDetailResult(this.msg, this.code, this.data);

  factory MatchingDetailResult.fromJson(Map<String, dynamic> json) {
    return MatchingDetailResult(
      json['msg'],
      json['code'],
      MatchingResponse.fromJson(json['data'])
    );
  }
}