// 게시판 복수용 큰틀

import 'package:travel_meeting_app/model/board_item.dart';

class BoardListResult {
  String msg;
  num code;
  List<BoardItem> list;
  num totalCount;

  BoardListResult(this.msg, this.code, this.list, this.totalCount);

  factory BoardListResult.fromJson(Map<String, dynamic> json) {
    return BoardListResult(
      json['msg'],
      json['code'],
      json['list'] != null ?
      (json['list'] as List).map((e) => BoardItem.fromJson(e)).toList()
          : [],
      json['totalCount'],
    );
  }
}