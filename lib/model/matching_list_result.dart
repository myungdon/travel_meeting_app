// 복수용 큰틀

import 'package:travel_meeting_app/model/matching_item.dart';

class MatchingListResult {
  String msg;
  num code;
  List<MatchingItem> list;
  num totalCount;

  MatchingListResult(this.msg, this.code, this.list, this.totalCount);

  factory MatchingListResult.fromJson(Map<String, dynamic> json) {
    return MatchingListResult(
      json['msg'],
      json['code'],
      json['list'] != null ?
      (json['list'] as List).map((e) => MatchingItem.fromJson(e)).toList()
          : [],
      json['totalCount'],
    );
  }
}