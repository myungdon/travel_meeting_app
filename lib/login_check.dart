import 'dart:async';

import 'package:flutter/material.dart';
import 'package:travel_meeting_app/middleware/middleware_login_check.dart';

class LoginCheck extends StatefulWidget {
  const LoginCheck({super.key});

  @override
  State<LoginCheck> createState() => _LoginCheckState();
}

class _LoginCheckState extends State<LoginCheck> {
  @override
  void initState(){
    super.initState();
    Timer(Duration(milliseconds: 2000), (){
      MiddlewareLoginCheck().check(context);
    });
  }

  // 스플래쉬 화면
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: null,
        body: Center(
            child: Image.asset(
              'assets/loading.png',
              fit: BoxFit.fill,
            )
        )
    );
  }
}