import 'package:flutter/material.dart';
import 'package:travel_meeting_app/components/main_divider.dart';
import 'package:travel_meeting_app/components/top_bar_detail.dart';
import 'package:travel_meeting_app/config/config_color.dart';
import 'package:travel_meeting_app/config/config_size.dart';

class PageMatchingPutin extends StatefulWidget {
  const PageMatchingPutin({super.key});

  @override
  State<PageMatchingPutin> createState() => _PageMatchingPutinState();
}

class _PageMatchingPutinState extends State<PageMatchingPutin> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _buildBody(context),
        bottomNavigationBar: BottomAppBar(
          child: Container(
            color: Colors.black,
            height: 50,
            child: ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, "/pay");
              },
              child: Text('신청하기'),
              style: ElevatedButton.styleFrom(
                minimumSize: Size(200, 50),
                primary: colorPrimary,
                onPrimary: colorSecondary,
              ),)
          ),
        )
    );
  }

  Widget _buildBody(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;
    double phoneHeight = MediaQuery.of(context).size.height;

      return SafeArea(
        child: Stack(
          children: [
            CustomScrollView(
              slivers: [
                TopBarDetail(),
                SliverList(
                  delegate: SliverChildListDelegate([
                    Image.asset(
                      'assets/description1.jpg',
                      width: phoneWidth,
                      height: phoneWidth / 1.5,
                      fit: BoxFit.fill,
                    ),
                    Container(
                      padding: EdgeInsets.all(20),
                      child: Text('우리는 만남을 원하고 바랍니다.\n일생 속에서 자연스러운 만남을 누구나 원하지만 현실은 다르더라고요.',
                          style: TextStyle(
                          fontSize: fontSizeMid
                      ),
                      ),
                    ),
                    Image.asset(
                      'assets/description2.jpg',
                      width: phoneWidth,
                      height: phoneHeight / 2,
                      fit: BoxFit.fill,
                    ),
                    Container(
                      padding: EdgeInsets.all(20),
                      child: Text('용기 있는 자가 애인을 얻는다고 했습니다\n신청을 고민하는 당신은 이미 큰 용기를 가진 사람입니다.',
                        style: TextStyle(
                            fontSize: fontSizeMid
                        ),
                      ),
                    ),
                    Image.asset(
                      'assets/description3.jpg',
                      width: phoneWidth,
                      height: phoneHeight / 2,
                      fit: BoxFit.fill,
                    ),
                    Container(
                      padding: EdgeInsets.all(20),
                      child: Text('저희 travelMatching은 당신의 용기에 더해 좋은 인연을 선물해 드리려 합니다.',
                        style: TextStyle(
                            fontSize: fontSizeMid
                        ),
                      ),
                    ),
                    Image.asset(
                      'assets/description4.jpg',
                      width: phoneWidth,
                      height: phoneHeight / 2,
                      fit: BoxFit.fill,
                    ),
                    Container(
                      padding: EdgeInsets.all(20),
                      child: Text('지금 당장 신청하세요. 희망찬 미래를 그리는 시작이 될 것입니다.',
                        style: TextStyle(
                            fontSize: fontSizeMid
                        ),
                      ),
                    ),
                    Image.asset(
                      'assets/description5.jpg',
                      width: phoneWidth,
                      height: phoneHeight / 1.5,
                      fit: BoxFit.fill,
                    ),
                    Container(
                      padding: EdgeInsets.all(30),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            '결혼을 간절히 원하는 솔로 남녀들이 모여 사랑을 찾기 위해 고군분투하는 극사실주의 데이팅 프로그램',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          SizedBox(height: 20),
                          MainDivider(),
                          SizedBox(height: 30,),
                          Text(
                            '지금 바로 신청하세요!',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ]),
                ),
              ],
            ),
          ],
        ),
      );
    }
 }

