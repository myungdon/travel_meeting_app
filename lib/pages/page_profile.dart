// 프로필 화면

import 'package:flutter/material.dart';
import 'package:travel_meeting_app/components/component_text_btn.dart';
import 'package:travel_meeting_app/components/loading.dart';
import 'package:travel_meeting_app/components/main_divider.dart';
import 'package:travel_meeting_app/components/top_bar.dart';
import 'package:travel_meeting_app/config/config_color.dart';
import 'package:travel_meeting_app/config/config_path.dart';
import 'package:travel_meeting_app/config/config_size.dart';
import 'package:travel_meeting_app/config/config_style.dart';
import 'package:travel_meeting_app/functions/token_lib.dart';
import 'package:travel_meeting_app/model/member_response.dart';
import 'package:travel_meeting_app/repo/repo_member.dart';


class PageProfile extends StatefulWidget {
  const PageProfile({super.key, required this.id});

  final num id;

  @override
  State<PageProfile> createState() => _PageProfileState();
}

class _PageProfileState extends State<PageProfile> {
  // MemberResponse? _detail;

  // Future<void> _loadDetail() async {
  //   await RepoMember().getMemberDetail(widget.id)
  //       .then((res) => {
  //     setState(() {
  //       _detail = res.data;
  //     })
  //   });
  // }

  // @override
  // void initState() {
  //   super.initState();
  //   _loadDetail();
  // }

  bool _isPush = false; // push 알람 변수

  var _border = UnderlineInputBorder( // 테두리 보이지 않게 하는 변수
      borderSide: BorderSide(
          color: Colors.transparent
      )
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _buildBody(context),
    );
  }

  Future<void> _logout() async {
    TokenLib.logout(context);
  }

  Widget _buildBody(BuildContext context) {
    Size _size = MediaQuery.of(context).size; // Media Query 변수

    // if (_detail == null){
    //   return Loading();
    // } else {
    {
      return SafeArea(
        child: Stack(
          children: [
            CustomScrollView(
              slivers: [
                TopBar(),
                SliverList(
                  delegate: SliverChildListDelegate([
                    LayoutBuilder(
                      builder: (context, constraints) {
                        // 프로필 사진
                        return Container(
                          padding: bodyPaddingAll,
                          child: Column(
                            children: [
                              Center(
                                child: InkWell(
                                  onTap: () async {
                                    showDialog(context: context,
                                        builder: (BuildContext context) =>
                                            SimpleDialog(
                                              children: [
                                                Image.asset(
                                                  '${pathBase}profile.jpg',
                                                  width: _size.width,
                                                  height: _size.height / 2,
                                                )
                                              ],
                                            ));
                                  },
                                  child: Container(
                                    child: Align(
                                      alignment: Alignment.center,
                                      child: CircleAvatar(
                                        backgroundImage: AssetImage(
                                            '${pathBase}profile.jpg'),
                                        radius: 70,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: 20),
                              Center(
                                child: Column(
                                  children: [
                                    Text('김남호', style: TextStyle(
                                        fontSize: fontSizeMid)),
                                    Text('브론즈')
                                  ],
                                ),
                              ),
                              Row(

                              )
                            ],
                          ),
                        );
                      },
                    ),
                    const MainDivider(),

                    Container(
                      padding: EdgeInsets.fromLTRB(20, 5, 20, 10),
                      child: Column(
                        children: [

                          // 마이 페이지
                          ListTile(title: Text('마이페이지', style: TextStyle(
                              fontSize: fontSizeSmall, color: colorThird),),),
                          const MainDivider(),
                          ListTile(
                            onTap: () {
                              Navigator.of(context).pushNamed(
                                  '/profile-modify');
                            },
                            title: Text('회원정보 수정'),
                            trailing: Icon(Icons.navigate_next),
                          ),
                          SizedBox(height: 20),

                          // 결제 내역 페이지
                          ListTile(title: Text('결제 내역', style: TextStyle(
                              fontSize: fontSizeSmall, color: colorThird),),),
                          const MainDivider(),
                          ListTile(
                            onTap: () {
                              Navigator.of(context).pushNamed('board-category');
                            },
                            title: Text('결제내역 보기'),
                            trailing: Icon(Icons.navigate_next),
                          ),
                          SizedBox(height: 20),

                          // 알림 설정
                          ListTile(title: Text('알림설정', style: TextStyle(
                              fontSize: fontSizeSmall, color: colorThird),),),
                          const MainDivider(),
                          ListTile(
                            onTap: () {
                              Navigator.of(context).pushNamed('board-category');
                            },
                            title: Text('푸시 알림'),
                            trailing: Switch(
                              value: this._isPush,
                              onChanged: (bool val) {
                                setState(() => this._isPush = val);
                              },
                            ),
                          ),
                          ListTile(
                            onTap: () {
                              Navigator.of(context).pushNamed('board-category');
                            },
                            title: Text('알림 목록'),
                            trailing: Icon(Icons.navigate_next),
                          ),
                          SizedBox(height: 20),

                          // 고객 센터
                          ListTile(title: Text('공지사항', style: TextStyle(
                              fontSize: fontSizeSmall, color: colorThird),),),
                          const MainDivider(),
                          ListTile(
                            onTap: () {
                              Navigator.of(context).pushNamed('board-category');
                            },
                            title: Text('공지사항'),
                            trailing: Icon(Icons.navigate_next),
                          ),

                          // 앱 정보
                          ListTile(title: Text('앱 기본 정보', style: TextStyle(
                              fontSize: fontSizeSmall, color: colorThird),),),
                          const MainDivider(),
                          ListTile(
                            onTap: () {
                              Navigator.of(context).pushNamed('board-category');
                            },
                            title: Text('앱 공유하기'),
                            trailing: Icon(Icons.navigate_next),
                          ),
                          ListTile(
                            onTap: () {
                              Navigator.of(context).pushNamed('board-category');
                            },
                            title: Text('버전체크'),
                            trailing: Text('1.01'),
                          ),
                          ListTile(
                            onTap: () {
                              Navigator.of(context).pushNamed('board-category');
                            },
                            title: Text('회원탈퇴'),
                            trailing: Icon(Icons.navigate_next),
                          ),
                          ListTile(
                            onTap: () {
                              showDialog(
                                  context: context,
                                  barrierDismissible: false,
                                  builder: (BuildContext context) =>
                                      AlertDialog(
                                        title: const Text('로그아웃'),
                                        content: const Text('로그아웃 하시겠습니까?'),
                                        actions: <Widget>[
                                          ComponentTextBtn('확인',
                                                  () async { _logout(); }),
                                          ComponentTextBtn('취소',
                                                  () { Navigator.of(context).pop(); },
                                            bgColor: Colors.black,
                                            borderColor: Colors.black,
                                          )
                                        ],
                                      ));
                            },
                            title: Text('로그아웃'),
                            trailing: Icon(Icons.navigate_next),
                          ),
                        ],
                      ),
                    )

                  ]),
                ),
              ],
            ),
          ],
        ),
      );
    }
  }
}