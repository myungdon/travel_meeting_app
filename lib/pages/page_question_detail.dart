// 게시판 자세히 보기 화면

import 'package:flutter/material.dart';
import 'package:travel_meeting_app/components/loading.dart';
import 'package:travel_meeting_app/components/main_divider.dart';
import 'package:travel_meeting_app/components/top_bar_detail.dart';
import 'package:travel_meeting_app/config/config_style.dart';
import 'package:travel_meeting_app/model/question_response.dart';
import 'package:travel_meeting_app/repo/repo_question.dart';

class PageQuestionDetail extends StatefulWidget {
  const PageQuestionDetail({super.key, required this.id});

  final num id;

  @override
  State<PageQuestionDetail> createState() => _PageQuestionDetailState();
}

class _PageQuestionDetailState extends State<PageQuestionDetail> {
  QuestionResponse? _detail;

  Future<void> _loadDetail() async {
    await RepoQuestion().getQuestion(widget.id)
        .then((res) => {
      setState(() {
        _detail = res.data;
      })
    });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _buildBody(context),
        bottomNavigationBar: BottomAppBar(
          child: Container(

          ),
        )
    );
  }

  Widget _buildBody(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;

    if (_detail == null) {
      return Loading();
    } else {
      return SafeArea(
        child: Stack(
          children: [
            CustomScrollView(
              slivers: [
                TopBarDetail(),
                SliverList(
                  delegate: SliverChildListDelegate([
                    Container(
                      padding: bodyPaddingAll,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            _detail!.title,
                            style: TextStyle(
                                fontSize: 26,
                                fontWeight: FontWeight.w700
                            ),
                          ),
                          SizedBox(height: 10,),
                          MainDivider(),
                          SizedBox(height: 10),
                          Text(
                            _detail!.content,
                            style: TextStyle(
                              fontSize: 16,
                              height: 1.583,
                            ),
                          ),
                          SizedBox(height: 10,),
                          MainDivider(),
                          SizedBox(height: 10,),
                          Text("작성일: ${_detail!.inputDay}"),
                        ],
                      ),
                    ),
                  ]),
                ),
              ],
            ),
          ],
        ),
      );
    }
  }
}