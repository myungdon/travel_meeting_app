// 게시판 자세히 보기 화면

import 'package:flutter/material.dart';
import 'package:travel_meeting_app/components/component_comments.dart';
import 'package:travel_meeting_app/components/loading.dart';
import 'package:travel_meeting_app/components/main_divider.dart';
import 'package:travel_meeting_app/components/top_bar_detail.dart';
import 'package:travel_meeting_app/config/config_style.dart';
import 'package:travel_meeting_app/model/comments_item.dart';
import 'package:travel_meeting_app/repo/repo_board.dart';
import 'package:travel_meeting_app/repo/repo_comments.dart';
import '../model/board_response.dart';

class PageBoardDetail extends StatefulWidget {
  const PageBoardDetail({super.key, required this.id});

  final num id;

  @override
  State<PageBoardDetail> createState() => _PageBoardDetailState();
}

class _PageBoardDetailState extends State<PageBoardDetail> {
  BoardResponse? _detail;
  // List<CommentsItem> _commentsList = [];

  Future<void> _loadDetail() async {
    await RepoBoard().getBoard(widget.id)
        .then((res) => {
      setState(() {
        _detail = res.data;
      })
    });
  }

  // Future<void> _loadCommentsList() async {
  //   await RepoComments().getComments()
  //       .then((res) =>
  //   {
  //     setState(() {
  //       _commentsList = res.list;
  //     })
  //   })
  //       .catchError((err) =>
  //   {
  //     debugPrint(err)
  //   });
  // }

  @override
  void initState() {
    super.initState();
    _loadDetail();
    // _loadCommentsList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _buildBody(context),
        bottomNavigationBar: BottomAppBar(
          child: Container(

          ),
        )
    );
  }

  Widget _buildBody(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;

    if (_detail == null) {
      return Loading();
    } else {
      return SafeArea(
        child: Stack(
          children: [
            CustomScrollView(
              slivers: [
                TopBarDetail(),
                SliverList(
                  delegate: SliverChildListDelegate([
                    Image.asset(
                      'assets/board4.jpg',
                      width: phoneWidth,
                      height: phoneWidth / 1.5,
                      fit: BoxFit.fill,
                    ),
                    Container(
                      padding: bodyPaddingAll,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            _detail!.title,
                            style: TextStyle(
                                fontSize: 26,
                                fontWeight: FontWeight.w700
                            ),
                          ),
                          Text('${_detail!.dateWrite.substring(0,10)} ${_detail!.dateWrite.substring(11,19)}'),
                          SizedBox(height: 10,),
                          MainDivider(),
                          SizedBox(height: 10),
                          Text(
                            _detail!.content,
                            style: TextStyle(
                              fontSize: 16,
                              height: 1.583,
                            ),
                          ),
                          SizedBox(height: 10,),
                          MainDivider(),
                          SizedBox(height: 10,),
                          SizedBox(
                            child: Text(
                              '댓글'
                            ),
                          ),
                          SizedBox(height: 10,),
                          MainDivider(),
                          SizedBox(height: 10,),
                          // ListView.builder(
                          //   itemCount: _commentsList.length,
                          //   itemBuilder: (BuildContext context, int idx) {
                          //     return ComponentComments(commentsItem: _commentsList[idx], callback: () {
                          //       // Navigator.of(context).push(MaterialPageRoute(
                          //       //     builder: (context) => PageBoardDetail(id: _commentsList[idx].id)));
                          //     });
                          //   },
                          // ),
                        ],
                      ),
                    ),
                  ]),
                ),
              ],
            ),
          ],
        ),
      );
    }
  }
}