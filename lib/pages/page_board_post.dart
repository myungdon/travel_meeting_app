// 게시글 작성 화면

import 'dart:typed_data';

import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:travel_meeting_app/components/main_divider.dart';
import 'package:travel_meeting_app/components/top_bar_detail.dart';
import 'package:travel_meeting_app/config/config_style.dart';

class PageBoardPost extends StatefulWidget {
  const PageBoardPost({super.key});

  @override
  State<PageBoardPost> createState() => _PageBoardPostState();
}

class _PageBoardPostState extends State<PageBoardPost> {
  List<Uint8List>? _images = []; // 대기 이미지용 변수
  bool _isPickingImages = false; // 버튼 클릭해서 이미지 가져오는 동안 로딩이 작동하는 변수

  var _border = UnderlineInputBorder( // 테두리 보이지 않게 하는 변수
      borderSide: BorderSide(
          color: Colors.transparent
      )
  );

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _buildBody(context),
        bottomNavigationBar: BottomAppBar(
          child: Container(
            color: Colors.black,
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed('/board');
                    },
                    child: Text('취소',)
                ),
                ElevatedButton(
                    onPressed: () {},
                    child: Text('완료',)
                )
              ],
            ),
          ),
        )
    );
  }

  Widget _buildBody(BuildContext context) {
    double _size = MediaQuery.of(context).size.width;

    return SafeArea(
      child: Stack(
        children: [
          CustomScrollView(
            slivers: [
              TopBarDetail(),
              SliverList(
                delegate: SliverChildListDelegate([
                  LayoutBuilder(
                    builder: (context, constraints) {
                      Size _size = MediaQuery.of(context).size; // Media Query 변수
                      var imgSize = _size.width / 3; // 이미지 사이즈 변수

                      // 앨범 이미지
                      return SizedBox(
                        height: imgSize,
                        width: _size.width,
                        child: ListView(
                          scrollDirection: Axis.horizontal, // 이미지 우측 스크롤 기능
                          children: [
                            Padding(
                              padding: bodyPaddingAll,
                              child: InkWell(
                                onTap: () async {

                                  _isPickingImages = true; // 버튼 클릭시 이미지 변수 로딩 시작

                                  final ImagePicker _picker = ImagePicker(); // 이미지 초기화 변수
                                  final List<XFile>? images = await _picker.pickMultiImage(imageQuality: 10); // 이미지 저장 변수 리소스 아끼려고 픽셀을 10%로 맞춘다

                                  if(images != null && images.isNotEmpty){ // 이미지가 null값이 아니고 비어있지 않다면
                                    _images!.clear(); // 이미지 클리어 하고
                                    images.forEach((xfile) async {_images!.add(await xfile.readAsBytes());}); // 각각 이미지에 맞게 바이트로 읽어서 저장

                                    _isPickingImages = false; // 이미지 변수 종료시 로딩 종료
                                    setState(() {

                                    });
                                  }
                                },
                                child: Container(
                                  width: _size.width / 4,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(16),
                                      border: Border.all(
                                          color: Colors.grey,
                                          width: 2
                                      )
                                  ),
                                  child: _isPickingImages?Padding( // 이미지 불러오는 동안 프로그래스바 표시 그게 아니면 카메라 모양
                                    padding: const EdgeInsets.all(5.0),
                                    child: CircularProgressIndicator(),
                                  ):Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      const Icon(Icons.camera_alt_rounded, color: Colors.grey,),
                                      Text('0/10',
                                        style: Theme.of(context).textTheme.titleSmall,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            ...List.generate(_images!.length, (index) => // 추가 이미지
                            Stack(
                              children: [
                                Padding(padding: const EdgeInsets.only(
                                  top: common_sm_padding,
                                  bottom: common_sm_padding,
                                  right: common_sm_padding
                                  ),
                                  child: ExtendedImage.memory(
                                    _images![index],
                                    height: imgSize,
                                    width: imgSize,
                                    fit: BoxFit.cover,
                                    shape: BoxShape.rectangle, // 모양
                                    borderRadius: BorderRadius.circular(16),
                                      loadStateChanged: (state) {

                                          switch(state.extendedImageLoadState) {

                                            case LoadState.loading: // 이미지를 불러 오는중
                                              return Container(
                                                padding: EdgeInsets.all(imgSize/3),
                                                height: imgSize,
                                                width: imgSize,
                                                child: CircularProgressIndicator(),
                                              );

                                            case LoadState.completed: // 로딩이 성공 했을 경우
                                              return null;

                                            case LoadState.failed: // 로딩이 실패 했을 경우
                                              return Icon(Icons.cancel);
                                          }
                                      },
                                    ),
                                  ),
                                Positioned( // 빼기 버튼
                                  right: 10,
                                  top: 10,
                                  child: IconButton(
                                    padding: EdgeInsets.zero,
                                      onPressed: () { // 불러온 이미지 삭제
                                      _images!.removeAt(index);
                                      setState(() {

                                      });
                                      },
                                      icon: Icon(
                                        Icons.remove_circle,
                                        size: 30,
                                        color: Colors.red[400],
                                      ),
                                  ),
                                )
                              ],
                             ),
                            )
                          ],
                        ),
                      );
                    },
                  ),
                  const MainDivider(),

                  // 글 작성
                  Container(
                    padding: bodyPaddingAll,
                    child: Column(
                      children: [
                        //게시글 제목
                        TextFormField(
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.symmetric(
                                horizontal: common_sm_padding
                            ),
                            hintText: '게시글 제목',
                            border: _border,
                            enabledBorder: _border,
                            focusedBorder: _border,
                          ),
                        ),
                        const MainDivider(),

                        // 카테고리 선택
                        ListTile(
                          onTap: () {
                            Navigator.of(context).pushNamed('board-category');
                          },
                          dense: true,
                          title: Text('카테고리 선택'),
                          trailing: Icon(Icons.navigate_next),
                        ),
                        const MainDivider(),

                        // 게시글 내용
                        TextFormField(
                          maxLines: null, // 텍스트 입력 가능한 줄
                          keyboardType: TextInputType.multiline, // 키보드에 완료 대신 스페이스바 생성
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.symmetric(
                                horizontal: common_sm_padding
                            ),
                            hintText: '내용을 입력해 주세요.',
                            border: _border,
                            enabledBorder: _border,
                            focusedBorder: _border,
                          ),
                        ),
                      ],
                    ),
                  )
                ]),
              ),
            ],
          ),
        ],
      ),
    );
  }
}