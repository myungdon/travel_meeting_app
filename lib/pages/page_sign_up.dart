// 회원 가입 화면

import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:travel_meeting_app/components/component_text_btn.dart';
import 'package:travel_meeting_app/components/main_divider.dart';
import 'package:travel_meeting_app/functions/member_lib.dart';
import 'package:travel_meeting_app/model/member_join_request.dart';
import 'package:travel_meeting_app/pages/page_login.dart';
import 'package:travel_meeting_app/repo/repo_member.dart';

class PageSignUp extends StatefulWidget {
  const PageSignUp({Key? key}) : super(key: key);

  @override
  State<PageSignUp> createState() => _PageSignUpState();
}

class _PageSignUpState extends State<PageSignUp> {
  final _formKey = GlobalKey<FormBuilderState>();

  // Future<void> _doSignUp(MemberJoinRequest memberJoinRequest) async {
  //   await RepoMember().setMember(memberJoinRequest)
  //       .then((res) {
  //     MemberLib.setMemberName(res.data.name);
  //     MemberLib.setMemberAccount(res.data.account);
  //     MemberLib.setMemberPassword(res.data.password);
  //     MemberLib.setMemberPasswordRe(res.data.passwordRe);
  //     MemberLib.setMemberBirthday(res.data.birthday);
  //     MemberLib.setMemberPhoneNumber(res.data.phoneNumber);
  //     MemberLib.setMemberIsMan(res.data.isMan);
  //     MemberLib.setMemberEMail(res.data.eMail);
  //     MemberLib.setMemberAddress(res.data.address);
  //     Navigator.pushAndRemoveUntil(
  //         context,
  //         MaterialPageRoute(
  //             builder: (BuildContext context) => const PageLogin()),
  //             (route) => false);
  //   }).catchError((err) {
  //     debugPrint(err);
  //   });
  // }

  @override
  void initState(){
    super.initState();
    // _loadData();
  }

  @override
  Widget build(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;
    DateTime date = DateTime.now(); // 생일 선택 변수

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Image.asset(
          'assets/logo.png',
          width: phoneWidth / 7,
        ), // 로고
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(children: [

            // 이름 입력칸
            Container(
              padding: EdgeInsets.all(5),
              margin: EdgeInsets.fromLTRB(70, 5, 70, 5),
              child: FormBuilderTextField(
                name: 'name',
                maxLength: 20,
                keyboardType: TextInputType.emailAddress,
                decoration: const InputDecoration(
                  prefixIcon: Icon(
                    Icons.account_circle,
                    color: Colors.blueGrey,
                  ),
                  labelText: '이름',
                  hintText: '이름을 입력해주세요',
                  labelStyle: TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                ),
              ),
            ),

            // ID 입력칸
            Container(
              padding: EdgeInsets.all(5),
              margin: EdgeInsets.fromLTRB(70, 5, 70, 5),
              child: FormBuilderTextField(
                obscureText: true,
                name: 'account',
                maxLength: 20,
                keyboardType: TextInputType.emailAddress,
                decoration: const InputDecoration(
                  prefixIcon: Icon(
                    Icons.account_circle,
                    color: Colors.blueGrey,
                  ),
                  labelText: 'ID',
                  hintText: 'ID 입력해주세요',
                  labelStyle: TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                ),
              ),
            ),

            // 비밀번호 입력칸
            Container(
              padding: EdgeInsets.all(5),
              margin: EdgeInsets.fromLTRB(70, 5, 70, 5),
              child: FormBuilderTextField(
                obscureText: true,
                name: 'password',
                maxLength: 20,
                keyboardType: TextInputType.emailAddress,
                decoration: const InputDecoration(
                  prefixIcon: Icon(
                    Icons.lock,
                    color: Colors.blueGrey,
                  ),
                  labelText: 'password',
                  hintText: 'password를  입력해주세요',
                  labelStyle: TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                ),
              ),
            ),

            // 비밀번호 재확인칸
            Container(
              padding: EdgeInsets.all(5),
              margin: EdgeInsets.fromLTRB(70, 5, 70, 5),
              child: FormBuilderTextField(
                obscureText: true,
                name: 'passwordRe',
                maxLength: 20,
                keyboardType: TextInputType.emailAddress,
                decoration: const InputDecoration(
                  prefixIcon: Icon(
                    Icons.lock,
                    color: Colors.blueGrey,
                  ),
                  labelText: 'password confirm',
                  hintText: '비밀번호를 재입력 해주세요',
                  labelStyle: TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                ),
              ),
            ),

            // 생일칸
            Container(
              padding: EdgeInsets.all(5),
              margin: EdgeInsets.fromLTRB(70, 5, 70, 5),
              child: FormBuilderTextField(
                name: 'birthday',
                maxLength: 15,
                keyboardType: TextInputType.emailAddress,
                // onTap: () async {
                //   final selectedDate = await showDatePicker(
                //     context: context,
                //     initialDate: date,
                //     firstDate: DateTime(1950),
                //     lastDate: DateTime.now(),
                //     initialEntryMode: DatePickerEntryMode.calendarOnly,
                //   );
                //   if (selectedDate != null) {
                //     setState(() {
                //       date = selectedDate;
                //     });
                //   }
                // },
                decoration: const InputDecoration(
                  prefixIcon: Icon(
                    Icons.calendar_month,
                    color: Colors.blueGrey,
                  ),
                  labelText: '생년월일',
                  hintText: '2000-01-01',
                  labelStyle: TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                ),
              ),
            ),

            // 핸드폰 번호 입력칸
            Container(
              padding: EdgeInsets.all(5),
              margin: EdgeInsets.fromLTRB(70, 5, 70, 5),
              child: FormBuilderTextField(
                name: 'phoneNumber',
                maxLength: 13,
                keyboardType: TextInputType.emailAddress,
                decoration: const InputDecoration(
                  prefixIcon: Icon(
                    Icons.smartphone,
                    color: Colors.blueGrey,
                  ),
                  labelText: '핸드폰 번호',
                  hintText: '010-0000-0000',
                  labelStyle: TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                ),
              ),
            ),

            // 성별 선택칸
            Container(
              padding: EdgeInsets.all(5),
              margin: EdgeInsets.fromLTRB(70, 5, 70, 5),
              child: FormBuilderDropdown<String?>(
                name: 'isMan',
                items: ['true', 'false'].map<DropdownMenuItem<String?>>((String? i) {
                  return DropdownMenuItem<String?>(
                    value: i,
                    child: Text({'true': '남성'}[i] ?? '여성'),
                  );
                }).toList(),
                decoration: const InputDecoration(
                  prefixIcon: Icon(
                    Icons.wc,
                    color: Colors.blueGrey,
                  ),
                  labelText: '성별',
                  labelStyle: TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                ),
              ),
            ),

            // 이메일 입력칸
            Container(
              padding: EdgeInsets.all(5),
              margin: EdgeInsets.fromLTRB(70, 5, 70, 5),
              child: FormBuilderTextField(
                name: 'eMail',
                maxLength: 50,
                keyboardType: TextInputType.emailAddress,
                decoration: const InputDecoration(
                  prefixIcon: Icon(
                    Icons.email,
                    color: Colors.blueGrey,
                  ),
                  labelText: 'Email 입력',
                  hintText: 'Email을 입력해주세요',
                  labelStyle: TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                ),
              ),
            ),

            // 주소입력칸
            Container(
              padding: EdgeInsets.all(5),
              margin: EdgeInsets.fromLTRB(70, 5, 70, 5),
              child: FormBuilderTextField(
                name: 'address',
                maxLength: 50,
                keyboardType: TextInputType.emailAddress,
                decoration: const InputDecoration(
                  prefixIcon: Icon(
                    Icons.location_pin,
                    color: Colors.blueGrey,
                  ),
                  labelText: '주소',
                  hintText: '주소를 입력해주세요',
                  labelStyle: TextStyle(color: Colors.black),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  ),
                ),
              ),
            ),
            MainDivider(),

            // 등록 버튼
            Container(
              margin: EdgeInsets.only(top: 30, bottom: 50),
              width: MediaQuery.of(context).size.width * 0.7,
              height: MediaQuery.of(context).size.height * 0.05,
              child: ComponentTextBtn('가입하기', () {
                if(_formKey.currentState!.saveAndValidate()) {
                  MemberJoinRequest memberJoinRequest = MemberJoinRequest(
                    _formKey.currentState!.fields['name']!.value,
                    _formKey.currentState!.fields['account']!.value,
                    _formKey.currentState!.fields['password']!.value,
                    _formKey.currentState!.fields['passwordRe']!.value,
                    _formKey.currentState!.fields['birthday']!.value,
                    _formKey.currentState!.fields['phoneNumber']!.value,
                    _formKey.currentState!.fields['isMan']!.value,
                    _formKey.currentState!.fields['eMail']!.value,
                    _formKey.currentState!.fields['address']!.value,
                  );
                  // _doSignUp(memberJoinRequest);
                  Navigator.pushNamed(context, '/index');
                }
              }
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
