// 매칭 전체 화면

import 'package:flutter/material.dart';
import 'package:travel_meeting_app/components/component_matching.dart';
import 'package:travel_meeting_app/components/loading.dart';
import 'package:travel_meeting_app/components/top_bar.dart';
import 'package:travel_meeting_app/model/matching_item.dart';
import 'package:travel_meeting_app/pages/page_matching_detail.dart';
import 'package:travel_meeting_app/repo/repo_matching.dart';


class PageMatching extends StatefulWidget {
  const PageMatching({super.key});

  @override
  State<PageMatching> createState() => _PageMatchingState();
}

class _PageMatchingState extends State<PageMatching> {
  List<MatchingItem> _matchingList = [
    MatchingItem(1, '양평 1기', '1.jpg', 'G2024-01-2577A', 400000, 'G', '2024-01-25', '2024-01-27', '77', 'A', '상세 내용'),
    MatchingItem(2, '양평 2기', '2.jpg', 'G2024-01-2577B', 400000, 'G', '2024-01-25', '2024-01-27', '77', 'A', '상세 내용'),
    MatchingItem(3, '부산 1기', '3.jpg', 'G2024-01-2577A', 400000, 'G', '2024-01-25', '2024-01-27', '33', 'A', '상세 내용'),
    MatchingItem(4, '서울 1기', '4.jpg', 'G2024-01-2577A', 400000, 'G', '2024-01-25', '2024-01-27', '44', 'A', '상세 내용'),
  ];

  // Future<void> _loadList() async {
  //   await RepoMatching().getMatching()
  //       .then((res) =>
  //   {
  //     setState(() {
  //       _matchingList = res.list;
  //     })
  //   })
  //       .catchError((err) =>
  //   {
  //     debugPrint(err)
  //   });
  // }

  // @override
  // void initState() {
  //   super.initState();
  //   _loadList();
  // }

  @override
  Widget build(BuildContext context) {
    if (_matchingList.isEmpty) {
      return Loading();
    } else {
      return Scaffold(
        body: SafeArea(
          child: DefaultTabController(
            length: 1, // 탭 개수
            child: CustomScrollView(
              slivers: [
                TopBar(),
                const SliverPersistentHeader(
                    pinned: true, delegate: TabBarDelegate()),
                SliverFillRemaining(
                  // 탭바 뷰 내부에는 스크롤이 되는 위젯이 들어옴.
                  hasScrollBody: true,
                  child: TabBarView(
                    children: [
                      ListView.builder(
                        itemCount: _matchingList.length,
                        itemBuilder: (BuildContext context, int idx) {
                          return ComponentMatching(
                              matchingItem: _matchingList[idx], callback: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) =>
                                    PageMatchingDetail(
                                        id: _matchingList[idx].id)));
                          });
                        },
                      ),
                      // ListView.builder(
                      //   itemCount: _matchingList.length,
                      //   itemBuilder: (BuildContext context, int idx) {
                      //     return ComponentMatchingRandom(matchingItem: _matchingList[idx], callback: () {
                      //       Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageMatchingDetail(id: _matchingList[idx].id)));
                      //     });
                      //   },
                      // ),
                      // ListView.builder(
                      //   itemCount: _matchingList.length,
                      //   itemBuilder: (BuildContext context, int idx) {
                      //     return ComponentMatchingGroup(matchingItem: _matchingList[idx], callback: () {
                      //       Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageMatchingDetail(id: _matchingList[idx].id)));
                      //     });
                      //   },
                      // ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      );
    }
  }
}
class TabBarDelegate extends SliverPersistentHeaderDelegate {
  const TabBarDelegate();


// 탭바 이름 관리
  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      color: Colors.white,
      child: TabBar(
        tabs: [
          Tab(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              color: Colors.white,
              child: const Text(
                "매칭",
              ),
            ),
          ),
          // Tab(
          //   child: Container(
          //     padding: const EdgeInsets.symmetric(horizontal: 8),
          //     color: Colors.white,
          //     child: const Text(
          //       "랜덤",
          //     ),
          //   ),
          // ),
          // Tab(
          //   child: Container(
          //     padding: const EdgeInsets.symmetric(horizontal: 8),
          //     color: Colors.white,
          //     child: const Text(
          //       "그룹",
          //     ),
          //   ),
          // ),
        ],
        indicatorWeight: 2,
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        unselectedLabelColor: Colors.grey,
        labelColor: Colors.black,
        indicatorColor: Colors.black,
        indicatorSize: TabBarIndicatorSize.label,
      ),
    );
  }

  @override
  double get maxExtent => 48;

  @override
  double get minExtent => 48;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}