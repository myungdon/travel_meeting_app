import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:travel_meeting_app/components/component_text_btn.dart';
import 'package:travel_meeting_app/components/main_divider.dart';
import 'package:travel_meeting_app/config/config_form_validator.dart';
import 'package:travel_meeting_app/functions/token_lib.dart';
import 'package:travel_meeting_app/middleware/middleware_login_check.dart';
import 'package:travel_meeting_app/model/login_request.dart';
import 'package:travel_meeting_app/repo/repo_member.dart';

class PageLogin extends StatefulWidget {
  const PageLogin({Key? key}) : super(key: key);

  @override
  State<PageLogin> createState() => _PageLoginState();
}

class _PageLoginState extends State<PageLogin> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _doLogin(LoginRequest loginRequest) async {
    await RepoMember().doLogin(loginRequest).then((res) {
      // api에서 받은 결과값을 token에 넣는다.
      TokenLib.setMemberToken(res.data.token);
      TokenLib.setMemberName(res.data.name);
      // 미들웨어에게 부탁해서 토큰값 여부 검사 후 페이지 이동을 부탁한다.
      MiddlewareLoginCheck().check(context);
    }).catchError((err) {
      debugPrint(err);
    });
  }

  @override
  Widget build(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(15),
        child: FormBuilder(
          key: _formKey,
          autovalidateMode: AutovalidateMode.disabled,
          child: ListView(
            children: [
              SizedBox(height:50),
              Image.asset(
                'assets/logo.png',
                width: phoneWidth / 1.2,
              ),
              idInput(),
              const SizedBox(height: 15,),
              passwordInput(),
              const SizedBox(height: 15),
              submitButton(),
              const SizedBox(height: 15),
              const MainDivider(),
              const SizedBox(height: 15),
              signUpButton()
            ],
          ),
        ),
      ),
    );
  }

  // ID 입력
  Widget idInput() {
    return FormBuilderTextField(
      name: 'account',
      maxLength: 20,
      keyboardType: TextInputType.text,
      autofocus: true,
      validator: FormBuilderValidators.compose([
        FormBuilderValidators.required(errorText: formErrorRequired),
        FormBuilderValidators.minLength(4, errorText: formErrorMinLength(4)),
        FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
      ]),
    decoration: const InputDecoration(
    border: OutlineInputBorder(),
    hintText: 'ID를 입력해주세요.',
    labelText: 'ID',
    labelStyle: TextStyle(
    fontSize: 18,
    fontWeight: FontWeight.bold,
    ),
    ),
    );
  }

  // 비밀번호 입력
  Widget passwordInput() {
    return FormBuilderTextField(
      obscureText: true,
      name: 'password',
      maxLength: 20,
      validator: FormBuilderValidators.compose([
        FormBuilderValidators.required(errorText: formErrorRequired),
        FormBuilderValidators.minLength(6, errorText: formErrorMinLength(6)),
        FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
      ]),
      decoration: const InputDecoration(
        border: OutlineInputBorder(),
        hintText: '비밀번호를 입력해주세요.',
        labelText: 'Password',
        labelStyle: TextStyle(
          fontSize: 18,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  // 로그인 버튼
  Widget submitButton() {
    return ComponentTextBtn('로그인', () {
      if(_formKey.currentState!.saveAndValidate()) {
        LoginRequest loginRequest = LoginRequest(
          _formKey.currentState!.fields['account']!.value,
          _formKey.currentState!.fields['password']!.value,
        );
        _doLogin(loginRequest);
      }
    }
    );
  }

  // 회원 가입 버튼
  Widget signUpButton() {
    return ComponentTextBtn('회원가입', () { Navigator.of(context).pushNamed('/sign-up'); });
  }
}
