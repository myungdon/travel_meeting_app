// 매칭 자세히 보기 화면

import 'package:flutter/material.dart';
import 'package:travel_meeting_app/components/loading.dart';
import 'package:travel_meeting_app/components/main_divider.dart';
import 'package:travel_meeting_app/components/top_bar_detail.dart';
import 'package:travel_meeting_app/config/config_style.dart';
import 'package:travel_meeting_app/model/matching_response.dart';
import 'package:travel_meeting_app/repo/repo_matching.dart';

class PageMatchingDetail extends StatefulWidget {
  const PageMatchingDetail({super.key, required this.id});

  final num id;

  @override
  State<PageMatchingDetail> createState() => _PageMatchingDetailState();
}

class _PageMatchingDetailState extends State<PageMatchingDetail> {
  MatchingResponse? _detail;

  Future<void> _loadDetail() async {
    await RepoMatching().getMatchingDetail(widget.id)
        .then((res) => {
      setState(() {
        _detail = res.data;
      })
    });
  }

  @override
  void initState() {
    super.initState();
    _loadDetail();
  }

  bool _isLiked = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _buildBody(context),
        bottomNavigationBar: BottomAppBar(
          child: Container(
            color: Colors.black,
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                IconButton(
                  icon: _isLiked
                      ? Icon(Icons.favorite, color: Colors.red,)
                      : Icon(Icons.favorite_border, color: Colors.white,),
                  onPressed: () {
                    setState(() {
                      _isLiked = !_isLiked;
                    });
                  },
                ),
                ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed('/pay');
                    },
                    child: Text('신청하기',)
                )
              ],
            ),
          ),
        )
    );
  }

  Widget _buildBody(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;

    if (_detail == null) {
      return Loading();
    } else {
      return SafeArea(
        child: Stack(
          children: [
            CustomScrollView(
              slivers: [
                TopBarDetail(),
                SliverList(
                  delegate: SliverChildListDelegate([
                    Image.asset(
                      _detail!.matchingImg,
                      width: phoneWidth,
                      height: phoneWidth / 1.5,
                      fit: BoxFit.fill,
                    ),
                    Container(
                      padding: EdgeInsets.all(30),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            _detail!.matchingName,
                            style: TextStyle(
                                fontSize: 26,
                                fontWeight: FontWeight.w700
                            ),
                          ),
                          Text(
                            '${_detail!.matchingPrice}원',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                          SizedBox(height: 10,),
                          MainDivider(),
                          SizedBox(height: 10),
                          Text(
                            _detail!.etcMemo,
                            style: TextStyle(
                              fontSize: 16,
                              height: 1.583,
                            ),
                          ),
                          SizedBox(height: 10,),
                          MainDivider(),
                          SizedBox(height: 10,),
                          Text('날짜 : ${_detail!.dateStart} ~ ${_detail!.dateEnd}'),
                        ],
                      ),
                    ),
                  ]),
                ),
              ],
            ),
          ],
        ),
      );
    }
  }
}