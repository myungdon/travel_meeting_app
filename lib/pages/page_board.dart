// 게시판 화면

import 'package:flutter/material.dart';
import 'package:travel_meeting_app/components/component_board.dart';
import 'package:travel_meeting_app/components/component_question.dart';
import 'package:travel_meeting_app/components/loading.dart';
import 'package:travel_meeting_app/components/top_bar.dart';
import 'package:travel_meeting_app/config/config_color.dart';
import 'package:travel_meeting_app/model/board_item.dart';
import 'package:travel_meeting_app/model/question_item.dart';
import 'package:travel_meeting_app/pages/page_board_detail.dart';
import 'package:travel_meeting_app/pages/page_question_detail.dart';
import 'package:travel_meeting_app/repo/repo_board.dart';
import 'package:travel_meeting_app/repo/repo_question.dart';

class PageBoard extends StatefulWidget {
  const PageBoard({super.key});

  @override
  State<PageBoard> createState() => _PageBoardState();
}

class _PageBoardState extends State<PageBoard> {
  List<BoardItem> _boardList = [
    // BoardItem(1, 'board1.jpg','김남호', '2024-02-07', '재밌었어요', 'ㅋㅋ'),
    // BoardItem(2, 'board2.jpg', '배병수', '2024-02-07', '여기 어떤데?', '----'),
    // BoardItem(3, 'board3.jpg', '김민정', '2024-02-07', '우리 결혼했어용', '----' ),
  ];
  List<QuestionItem> _questionList = [
    QuestionItem(1, '김남호1', '궁금 해요', 'ㅋㅋ', '2024-02-07'),
    QuestionItem(2, '김남호2', 'zzz', 'ㅋㅋ', '2024-02-07'),
    QuestionItem(3, '김남호3', 'xxx', 'ㅋㅋ', '2024-02-07'),
  ];

  Future<void> _loadBoardList() async {
    await RepoBoard().getBoards()
        .then((res) =>
    {
      setState(() {
        _boardList = res.list;
      })
    })
        .catchError((err) =>
    {
      debugPrint(err)
    });
  }

  Future<void> _loadQuestionList() async {
    await RepoQuestion().getQuestions()
        .then((res) =>
    {
      setState(() {
        _questionList = res.list;
      })
    })
        .catchError((err) =>
    {
      debugPrint(err)
    });
  }

  @override
  void initState() {
    super.initState();
    _loadBoardList();
    _loadQuestionList();
  }


  @override
  Widget build(BuildContext context) {

    if (_boardList.isEmpty) {
      return Loading();
    } else {
      return Scaffold(
        body: SafeArea(
          child: DefaultTabController(
            length: 2, // 탭 개수
            child: CustomScrollView(
              slivers: [
                TopBar(),
                const SliverPersistentHeader(
                    pinned: true, delegate: TabBarDelegate()),
                SliverFillRemaining( // 탭바 뷰 내부에는 스크롤이 되는 위젯이 들어옴.
                  hasScrollBody: true,
                  child: Container(
                    margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
                    child: TabBarView(
                      children: [
                        // 매칭후기
                        ListView.builder(
                          itemCount: _boardList.length,
                          itemBuilder: (BuildContext context, int idx) {
                            return ComponentBoard(boardItem: _boardList[idx], callback: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => PageBoardDetail(id: _boardList[idx].id)));
                            });
                          },
                        ),

                        // 1:1 문의
                        ListView.builder(
                          itemCount: _questionList.length,
                          itemBuilder: (BuildContext context, int idx) {
                            return ComponentQuestion(questionItem: _questionList[idx], callback: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => PageQuestionDetail(id: _questionList[idx].id)));
                            });
                          },
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        // 글쓰기 버튼
        floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            Navigator.of(context).pushNamed('/board-post');
          },
          icon: Icon(Icons.edit),
          label: Text('글쓰기'),
          elevation: 0,
          foregroundColor: colorSecondary,
          backgroundColor: colorPrimary,
        ),
      );
    }
  }
}

class TabBarDelegate extends SliverPersistentHeaderDelegate {
  const TabBarDelegate();

// 탭바 이름 관리
  @override
  Widget build(BuildContext context, double shrinkOffset,
      bool overlapsContent) {
    return Container(
      color: Colors.white,
      child: TabBar(
        tabs: [
          Tab(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              color: Colors.white,
              child: const Text(
                "매칭 후기",
              ),
            ),
          ),
          Tab(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              color: Colors.white,
              child: const Text(
                "1:1문의",
              ),
            ),
          ),
        ],
        indicatorWeight: 2,
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        unselectedLabelColor: Colors.grey,
        labelColor: Colors.black,
        indicatorColor: Colors.black,
        indicatorSize: TabBarIndicatorSize.label,
      ),
    );
  }

  @override
  double get maxExtent => 48;

  @override
  double get minExtent => 48;

  @override
  bool shouldRebuild(covariant SliverPersistentHeaderDelegate oldDelegate) {
    return false;
  }
}