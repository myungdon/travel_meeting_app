// 결제 완료 화면

import 'package:flutter/material.dart';
import 'package:travel_meeting_app/components/component_text_btn.dart';
import 'package:travel_meeting_app/components/loading.dart';
import 'package:travel_meeting_app/components/main_divider.dart';
import 'package:travel_meeting_app/components/top_bar.dart';
import 'package:travel_meeting_app/config/config_color.dart';
import 'package:travel_meeting_app/config/config_path.dart';
import 'package:travel_meeting_app/config/config_size.dart';
import 'package:travel_meeting_app/config/config_style.dart';
import 'package:travel_meeting_app/functions/token_lib.dart';
import 'package:travel_meeting_app/model/member_response.dart';
import 'package:travel_meeting_app/repo/repo_member.dart';


class PagePayDone extends StatefulWidget {
  const PagePayDone({super.key});

  @override
  State<PagePayDone> createState() => _PagePayDoneState();
}

class _PagePayDoneState extends State<PagePayDone> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(context),
        bottomNavigationBar: BottomAppBar(
          child: Container(
            color: Colors.black,
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                // 결제 버튼
                TextButton(
                  onPressed: () {
                    Navigator.pushNamed(context, "/index");
                  },
                  child: const Text(
                    '홈으로',
                    style: TextStyle(
                        color: colorSecondary
                    ),
                  ),
                )
              ],
            ),
          ),
        )
    );
  }

  Widget _buildBody(BuildContext context) {
    Size _size = MediaQuery.of(context).size; // Media Query 변수

      return SafeArea(
        child: Stack(
          children: [
            CustomScrollView(
              slivers: [
                SliverList(
                  delegate: SliverChildListDelegate([
                    LayoutBuilder(
                      builder: (context, constraints) {
                        // 프로필 사진
                        return Container(
                          margin: EdgeInsets.only(top: 200),
                          padding: bodyPaddingAll,
                          child: Column(
                            children: [
                              Center(
                                child: Container(
                                    child: Align(
                                      alignment: Alignment.center,
                                      child: Image.asset('${pathBase}pay_done.jpg')
                                    ),
                                  ),
                              ),
                              SizedBox(height: 20),
                              Center(
                                child: Column(
                                  children: [
                                    Text('결제가 완료 되었습니다.', style: TextStyle(
                                        fontSize: fontSizeMid)),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ]),
                ),
              ],
            ),
          ],
        ),
      );
  }
}