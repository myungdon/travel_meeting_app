// 홈 화면

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:travel_meeting_app/components/component_home_board.dart';
import 'package:travel_meeting_app/components/component_home_matching.dart';
import 'package:travel_meeting_app/components/component_text_btn.dart';
import 'package:travel_meeting_app/components/loading.dart';
import 'package:travel_meeting_app/components/top_bar.dart';
import 'package:travel_meeting_app/config/config_color.dart';
import 'package:travel_meeting_app/model/board_item.dart';
import 'package:travel_meeting_app/model/matching_item.dart';
import 'package:travel_meeting_app/pages/page_board_detail.dart';
import 'package:travel_meeting_app/pages/page_matching_detail.dart';
import 'package:travel_meeting_app/repo/repo_board.dart';
import 'package:travel_meeting_app/repo/repo_matching.dart';

class PageHome extends StatefulWidget {
  const PageHome({super.key});

  @override
  State<PageHome> createState() => _PageHomeState();
}

class _PageHomeState extends State<PageHome> {
  // 스크롤 이미지 변수
  var activeIndex = 0;

  // 배너 이미지 변수
  List imageList = [
    "assets/banner1.jpg",
    "assets/banner2.jpg",
    "assets/banner3.jpg",
    "assets/banner4.jpg",
  ];

  List<MatchingItem> _matchingList = [
    MatchingItem(1, '양평 1기', '1.jpg', 'G2024-01-2577A', 400000, 'G', '2024-01-25', '2024-01-27', '77', 'A', '상세 내용'),
    MatchingItem(2, '양평 2기', '2.jpg', 'G2024-01-2577B', 400000, 'G', '2024-01-25', '2024-01-27', '77', 'A', '상세 내용'),
    MatchingItem(3, '부산 1기', '3.jpg', 'G2024-01-2577A', 400000, 'G', '2024-01-25', '2024-01-27', '33', 'A', '상세 내용'),
    MatchingItem(4, '서울 1기', '4.jpg', 'G2024-01-2577A', 400000, 'G', '2024-01-25', '2024-01-27', '44', 'A', '상세 내용'),
  ];
  
  List<BoardItem> _boardList = [
    BoardItem(1, 'board1.jpg','김남호', '2024-02-07', '재밌었어요', 'ㅋㅋ'),
    BoardItem(2, 'board2.jpg', '배병수', '2024-02-07', '여기 어떤데?', '----'),
    BoardItem(3, 'board3.jpg', '김민정', '2024-02-07', '우리 결혼했어용', '----'),
  ];

  Future<void> _loadMatchingList() async {
    await RepoMatching().getMatching()
        .then((res) =>
    {
      setState(() {
        _matchingList = res.list;
      })
    })
        .catchError((err) => {
      debugPrint(err)
    });
  }

  Future<void> _loadBoardList() async {
    await RepoBoard().getBoards()
        .then((res) =>
    {
      setState(() {
        _boardList = res.list;
      })
    })
        .catchError((err) => {
      debugPrint(err)
    });
  }
  @override
  void initState() {
    super.initState();
    // _loadMatchingList();
    // _loadBoardList();
  }

  Widget imageSlider(path, index) => Container(
    width: double.infinity,
    height: 240,
    color: Colors.grey,
    child: Image.asset(path, fit: BoxFit.cover),
  );

  Widget indicator() => Container(
      margin: const EdgeInsets.only(bottom: 20.0),
      alignment: Alignment.bottomCenter,
      child: AnimatedSmoothIndicator(
        activeIndex: activeIndex,
        count: imageList.length,
        effect: JumpingDotEffect(
            dotHeight: 6,
            dotWidth: 6,
            activeDotColor: Colors.white,
            dotColor: Colors.white.withOpacity(0.6)),
      ));

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context){
    double phoneWidth = MediaQuery.of(context).size.width;

    if (_matchingList.isEmpty || _boardList.isEmpty) {
      return Loading();
    } else {
      return SafeArea(
        child: Stack(
          children: [
            CustomScrollView(
              slivers: [
                TopBar(),
                SliverToBoxAdapter(
                  child: Column(
                    children: [
                      Container(
                          child: Stack(alignment: Alignment.bottomCenter,
                              children: <Widget>[ // 스크롤 이미지
                                CarouselSlider.builder(
                                  options: CarouselOptions(
                                    autoPlay: true,
                                    autoPlayInterval: const Duration(
                                        seconds: 3),
                                    initialPage: 0,
                                    viewportFraction: 1,
                                    enlargeCenterPage: true,
                                    onPageChanged: (index, reason) =>
                                        setState(() {
                                          activeIndex = index;
                                        }),
                                  ),
                                  itemCount: imageList.length,
                                  itemBuilder: (context, index, realIndex) {
                                    final path = imageList[index];
                                    return imageSlider(path, index);
                                  },
                                ),
                                Align(alignment: Alignment.bottomCenter,
                                    child: indicator())
                              ])
                      ),
                      SizedBox(height: 30,),
                      // Container(
                      //   margin: EdgeInsets.only(left: 20, right: 20, top: 30),
                      //   child: Column(
                      //     children: [
                      //       Container(
                      //         padding: EdgeInsets.fromLTRB(0, 20, 0, 10),
                      //         child: Row(
                      //           mainAxisAlignment: MainAxisAlignment
                      //               .spaceBetween,
                      //           children: [
                      //             const Text(
                      //               '매칭',
                      //               style: TextStyle(
                      //                 fontSize: 25,
                      //                 fontWeight: FontWeight.w600,
                      //               ),
                      //             ),
                      //             TextButton(
                      //               child: const Text(
                      //                 '더보기',
                      //                 style: TextStyle(
                      //                   fontSize: 14,
                      //                   color: Colors.grey,
                      //                 ),
                      //                 textAlign: TextAlign.right,
                      //               ),
                      //               onPressed: () {
                      //                 Navigator.of(context).pushNamed(
                      //                     '/matching');
                      //               },
                      //             ),
                      //           ],
                      //         ),
                      //       ),
                      //     ],
                      //   ),
                      // ),
                      // Container(
                      //   height: phoneWidth / 3,
                      //   margin: EdgeInsets.fromLTRB(20, 10, 20, 20),
                      //   child:
                      //   ListView.builder(
                      //     shrinkWrap: true,
                      //     scrollDirection: Axis.horizontal,
                      //     itemCount: 4,
                      //     itemBuilder: (BuildContext context, int idx) {
                      //       return ComponentHomeMatching(
                      //           matchingItem: _matchingList[idx], callback: () {
                      //         Navigator.of(context).push(MaterialPageRoute(
                      //             builder: (context) =>
                      //                 PageMatchingDetail(
                      //                     id: _matchingList[idx].id)));
                      //       }
                      //       );
                      //     },
                      //   ),
                      // ),
                      Container(
                        margin: EdgeInsets.only(left: 20, right: 20),
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.fromLTRB(0, 20, 0, 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment
                                    .spaceBetween,
                                children: [
                                  const Text(
                                    '후기',
                                    style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                  TextButton(
                                    child: const Text(
                                      '더보기',
                                      style: TextStyle(
                                        fontSize: 14,
                                        color: Colors.grey,
                                      ),
                                      textAlign: TextAlign.right,
                                    ),
                                    onPressed: () {
                                      Navigator.of(context).pushNamed(
                                          '/matching');
                                    },
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: phoneWidth / 3,
                        margin: EdgeInsets.fromLTRB(20, 10, 20, 20),
                        child:
                        ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: 3,
                          itemBuilder: (BuildContext context, int idx) {
                            return ComponentHomeBoard(
                                boardItem: _boardList[idx], callback: () {
                              Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) =>
                                      PageBoardDetail(id: _boardList[idx].id)));
                            }
                            );
                          },
                        ),
                      ),
                      SizedBox(height: 30,),
                      ElevatedButton(
                        onPressed: () {
                          Navigator.pushNamed(context, "/matching-putin");
                        },
                        child: Text('매칭 신청'),
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(200, 60),
                          primary: colorPrimary,
                          onPrimary: colorSecondary,
                        ),),
                      // SizedBox(height: 30,)
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      );
    }
  }
}

