// 결제 화면

import 'package:flutter/material.dart';
import 'package:travel_meeting_app/components/top_bar_detail.dart';
import 'package:travel_meeting_app/config/config_color.dart';
import 'package:travel_meeting_app/config/config_size.dart';
import 'package:travel_meeting_app/pages/page_pay_done.dart';

class PagePay extends StatefulWidget {
  const PagePay({super.key});

  @override
  State<PagePay> createState() => _PagePayState();
}

enum payBy { CARD, KAKAO_PAY, MAKE_DEPOSIT, PHONE }

class _PagePayState extends State<PagePay> {
  payBy _payBy = payBy.CARD;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _buildBody(context),
        bottomNavigationBar: BottomAppBar(
          child: Container(
            color: Colors.black,
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                // 결제 버튼
                ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: colorPrimary),
                  onPressed: () {
                    showDialog<String>(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                        title: const Text('결제확인'),
                        content: const Text(
                          '결제 하시겠습니까?',
                        ),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () => Navigator.pop(context, '취소'),
                            child: const Text('취소'),
                          ),
                          TextButton(
                            onPressed: () => Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => PagePayDone()), (route) => false),
                            child: const Text('확인'),
                          ),
                        ],
                      ),
                    ).then((returnVal) {
                      if (returnVal != null) {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            content: Text('결과: $returnVal'),
                            action:
                                SnackBarAction(label: 'OK', onPressed: () {}),
                          ),
                        );
                      }
                    });
                  },
                  child: const Text(
                      '결제하기',
                    style: TextStyle(
                      color: colorSecondary
                    ),
                  ),
                )
              ],
            ),
          ),
        ));
  }

  Widget _buildBody(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;
    double phoneHeight = MediaQuery.of(context).size.height;

    return CustomScrollView(
      slivers: [
        TopBarDetail(),
        SliverToBoxAdapter(
          child: SingleChildScrollView(
            child: Container(
              margin: EdgeInsets.all(phoneWidth / 15),
              child: Column(
                children: [
                  // 결제 화면
                  Container(
                    // height: phoneHeight - 60,
                    width: phoneWidth,
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                      color: Colors.white,
                    ),
                    // 결제 수단
                    child: Container(
                      margin: EdgeInsets.all(phoneWidth / 20),
                      child: Column(
                        children: <Widget>[
                          const Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                '결제수단',
                                style: TextStyle(
                                  fontSize: fontSizeSuper,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              Text(
                                '400000원',
                                style: TextStyle(
                                  fontSize: fontSizeBig,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                            ],
                          ),
                          SizedBox(height: 20),
                          Divider(height: 1),
                          RadioListTile(
                              title: Text('카드 결제'),
                              value: payBy.CARD,
                              groupValue: _payBy,
                              onChanged: (value) {
                                setState(() {
                                  _payBy = value!;
                                });
                              }),
                          Divider(height: 1),
                          RadioListTile(
                              title: Text('카카오페이'),
                              value: payBy.KAKAO_PAY,
                              groupValue: _payBy,
                              onChanged: (value) {
                                setState(() {
                                  _payBy = value!;
                                });
                              }),
                          Divider(height: 1),
                          RadioListTile(
                              title: Text('무통장입금'),
                              value: payBy.MAKE_DEPOSIT,
                              groupValue: _payBy,
                              onChanged: (value) {
                                setState(() {
                                  _payBy = value!;
                                });
                              }),
                          Divider(height: 1),
                          RadioListTile(
                              title: Text('핸드폰 결제'),
                              value: payBy.PHONE,
                              groupValue: _payBy,
                              onChanged: (value) {
                                setState(() {
                                  _payBy = value!;
                                });
                              }),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: phoneWidth,
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(color: Colors.white),
                    child: Container(
                      margin: EdgeInsets.all(phoneWidth / 20),
                      child: const Column(
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                '결제상세',
                                style: TextStyle(
                                  fontSize: fontSizeSuper,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              SizedBox(height: 20),
                              Divider(height: 1),
                              SizedBox(height: 10),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('총 금액'),
                                  Text('400000원')
                                ],
                              ),
                              SizedBox(height: 10),
                              Divider(height: 1),
                              SizedBox(height: 10),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('포인트 사용'),
                                  Text('0원')
                                ],
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}
