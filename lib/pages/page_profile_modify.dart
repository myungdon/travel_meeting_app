// 회원정보 수정 화면

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:travel_meeting_app/components/main_divider.dart';
import 'package:travel_meeting_app/components/top_bar_detail.dart';
import 'package:travel_meeting_app/config/config_color.dart';
import 'package:travel_meeting_app/config/config_path.dart';
import 'package:travel_meeting_app/config/config_size.dart';
import 'package:travel_meeting_app/config/config_style.dart';

class PageProfileModify extends StatefulWidget {
  const PageProfileModify({super.key});


  @override
  State<PageProfileModify> createState() => _PageProfileModifyState();
}

class _PageProfileModifyState extends State<PageProfileModify> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: _buildBody(context),
        bottomNavigationBar: BottomAppBar(
          child: Container(
            color: Colors.black,
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed('/profile');
                    },
                    child: Text('취소',)
                ),
                ElevatedButton(
                    onPressed: () {},
                    child: Text('완료',)
                )
              ],
            ),
          ),
        )
    );
  }

  Widget _buildBody(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;
    double phoneHeight = MediaQuery.of(context).size.height;

    return SafeArea(
      child: Stack(
        children: [
          CustomScrollView(
            slivers: [
              TopBarDetail(),
              SliverList(
                delegate: SliverChildListDelegate([
                  LayoutBuilder(
                    builder: (context, constraints) {
                      Size phoneWidth = MediaQuery
                          .of(context)
                          .size; // Media Query 변수

                      // 앨범 이미지
                      return Container(
                        padding: bodyPaddingAll,
                        child: Center(
                          child: Stack(
                            children: [
                              Container(
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: CircleAvatar(
                                      backgroundImage: AssetImage(
                                          '${pathBase}profile.jpg'),
                                      radius: 70,
                                    ),
                                  )
                              ),
                              Positioned(
                                bottom: 0,
                                left: 120,
                                child: InkWell(
                                  onTap: () async {
                                    final ImagePicker _picker = ImagePicker(); // 이미지 초기 변수
                                    final List<XFile>? images = await _picker.pickMultiImage(imageQuality: 10); // 이미지 저장 변수 리소스 아끼려고 픽셀을 10%로 맞춘다
                                  },
                                  child: Container(
                                    width: phoneWidth.width / 4,
                                    height: phoneHeight / 30,
                                    color: colorThird,
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Text('이미지 변경',
                                          style: TextStyle(fontSize: fontSizeMid, color: colorSecondary)
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                  const MainDivider(),

                  // 정보 수정
                  Container(
                    padding: bodyPaddingAll,
                    child: Column(
                      children: [
                        //게시글 제목
                        TextFormField(
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.symmetric(
                                horizontal: common_sm_padding
                            ),
                            hintText: '이름',
                          ),
                        ),
                        const MainDivider(),

                        TextFormField(
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.symmetric(
                                horizontal: common_sm_padding
                            ),
                            hintText: '현재 비밀번호 입력',
                          ),
                        ),
                        const MainDivider(),

                        TextFormField(
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.symmetric(
                                horizontal: common_sm_padding
                            ),
                            hintText: '새 비밀번호',
                          ),
                        ),
                        const MainDivider(),

                        TextFormField(
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.symmetric(
                                horizontal: common_sm_padding
                            ),
                            hintText: '새 비밀번호 확인',
                          ),
                        ),
                        const MainDivider(),

                        TextFormField(
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.symmetric(
                                horizontal: common_sm_padding
                            ),
                            hintText: '핸드폰 번호',
                          ),
                        ),
                        const MainDivider(),

                        TextFormField(
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.symmetric(
                                horizontal: common_sm_padding
                            ),
                            hintText: '이메일',
                          ),
                        ),
                        const MainDivider(),

                        TextFormField(
                          decoration: InputDecoration(
                            contentPadding: const EdgeInsets.symmetric(
                                horizontal: common_sm_padding
                            ),
                            hintText: '주소',
                          ),
                        ),
                      ],
                    ),
                  )
                ]),
              ),
            ],
          ),
        ],
      ),
    );
  }
}