// import 'package:beamer/beamer.dart';
// import 'package:flutter/material.dart';
// import 'package:travel_meeting_app/components/main_divider.dart';
// import 'package:travel_meeting_app/states/category_board.dart';
//
// class PageBoardCategorySelect extends StatelessWidget {
//   const PageBoardCategorySelect({super.key});
//
//   @override
//   Widget build(BuildContext context) {
//     const List<String> categories = ['선택', '여행지 추천', '매칭 후기'];
//
//     return Scaffold(
//       appBar: AppBar(
//         title: Text(
//           '게시글 카테고리 선택',
//           style: Theme.of(context).textTheme.headlineSmall,
//         ),
//         centerTitle: true,
//       ),
//       body: ListView.separated(
//           itemBuilder: (context, index) { // 리스트 형식으로 아이템 배치
//             return ListTile(
//               onTap: (){
//                 // context.read<CategoryBoard>().setNewCategoryWithKor(
//                 //     categoriesMapEngToKor.values.elementAt(index));
//                 // Beamer.of(context).beamBack();
//               },
//                 title: Text(
//                     categoriesMapEngToKor.values.elementAt(index),
//                   // style: TextStyle(
//                   //   color: context.read<CategoryBoard>().setNewCategoryWithKor == categoriesMapEngToKor.values.elementAt(index)
//                   //       ?Theme.of(context).primaryColor
//                   //       :Colors.black
//                   // ),
//                 )
//             );
//           },
//           separatorBuilder: (context, index) { // 1개의 아이템 구분
//             return MainDivider();
//           },
//           itemCount: 3), // 리스트에 보여줄 아이템 갯수
//     );
//   }
// }
