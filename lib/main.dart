import 'package:flutter/material.dart';
import 'package:travel_meeting_app/login_check.dart';
import 'package:travel_meeting_app/pages/page_board.dart';
import 'package:travel_meeting_app/pages/page_board_post.dart';
import 'package:travel_meeting_app/pages/page_home.dart';
import 'package:travel_meeting_app/pages/page_index.dart';
import 'package:travel_meeting_app/pages/page_login.dart';
import 'package:travel_meeting_app/pages/page_matching.dart';
import 'package:travel_meeting_app/pages/page_matching_putin.dart';
import 'package:travel_meeting_app/pages/page_pay.dart';
import 'package:travel_meeting_app/pages/page_pay_done.dart';
import 'package:travel_meeting_app/pages/page_profile.dart';
import 'package:travel_meeting_app/pages/page_profile_modify.dart';
import 'package:travel_meeting_app/pages/page_sign_up.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: "/index",  // 시작 화면 주소 적는 곳
      routes: { // 페이지 주소 경로
        "/index": (context) => PageIndex(),
        "/home": (context) => PageHome(),
        "/login": (context) => PageLogin(),
       "/matching": (context) => PageMatching(),
        "/matching-putin": (context) => PageMatchingPutin(),
        "/board": (context) => PageBoard(),
        "/board-post": (context) => PageBoardPost(),
        "/pay": (context) => PagePay(),
        "/pay-done": (context) => PagePayDone(),
        // "/profile": (context) => PageProfile(id: 1,),
        "/profile-modify": (context) => PageProfileModify(),
        "/sign-up": (context) => PageSignUp(),
        "/login-check": (context) => LoginCheck(),
      },
      debugShowCheckedModeBanner: false, // 디버그 리본 삭제
      title: '여행팅',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
    );
  }
}