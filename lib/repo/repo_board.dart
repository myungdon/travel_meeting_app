// 게시판 레포

import 'package:dio/dio.dart';
import 'package:travel_meeting_app/config/config_api.dart';
import 'package:travel_meeting_app/functions/token_lib.dart';
import 'package:travel_meeting_app/model/board_detail_result.dart';
import 'package:travel_meeting_app/model/board_list_result.dart';

class RepoBoard {
  // 게시글 전체 불러오기
  Future<BoardListResult> getBoards() async {
    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/board/all';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return BoardListResult.fromJson(response.data);
  }

  // 게시글 하나 불러 오기
  Future<BoardDetailResult> getBoard(num boardId) async {
    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();

    String _baseUrl = '$apiUri/board/detail/{boardId}'; // 주소 확인

    final response = await dio.get(
        _baseUrl.replaceAll('{boardId}', boardId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (state) {
              return state == 200;
            }
        )
    );

    return BoardDetailResult.fromJson(response.data);
  }

  // 게시글 등록

}