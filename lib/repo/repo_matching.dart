// 매칭 레포

import 'package:dio/dio.dart';
import 'package:travel_meeting_app/config/config_api.dart';
import 'package:travel_meeting_app/functions/token_lib.dart';
import 'package:travel_meeting_app/model/matching_detail_result.dart';
import 'package:travel_meeting_app/model/matching_list_result.dart';

class RepoMatching {
  // 매칭 전체 보기
  Future<MatchingListResult> getMatching() async {
    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();

    String _baseUrl = '$apiUri/matching/all';

    final response = await dio.get(
        _baseUrl,
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );

    return MatchingListResult.fromJson(response.data);
  }

  // 매칭 하나 불러 오기
  Future<MatchingDetailResult> getMatchingDetail(num matchingId) async {
    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();

    String _baseUrl = '$apiUri/matching/detail/{matchingId}';

    final response = await dio.get(
     _baseUrl.replaceAll('{matchingId}', matchingId.toString()),
      options: Options(
        followRedirects: false,
        validateStatus: (state) {
          return state == 200;
        }
      )
    );

    return MatchingDetailResult.fromJson(response.data);
  }

  // 매칭 신청 취소
  Future<MatchingDetailResult> delMatching(num matchingId) async {
    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();

    String _baseUrl = '$apiUri/matching/{matchingId}';

    final response = await dio.get(
        _baseUrl.replaceAll('{matchingId}', matchingId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (state) {
              return state == 200;
            }
        )
    );

    return MatchingDetailResult.fromJson(response.data);
  }
}