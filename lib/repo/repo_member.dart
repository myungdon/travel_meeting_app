// 멤버 레포

import 'package:dio/dio.dart';
import 'package:travel_meeting_app/config/config_api.dart';
import 'package:travel_meeting_app/functions/token_lib.dart';
import 'package:travel_meeting_app/model/common_result.dart';
import 'package:travel_meeting_app/model/login_request.dart';
import 'package:travel_meeting_app/model/login_result.dart';
import 'package:travel_meeting_app/model/member_detail_result.dart';
import 'package:travel_meeting_app/model/member_join_request.dart';
import 'package:travel_meeting_app/model/member_join_result.dart';

class RepoMember {
  // 멤버 정보 불러오기
  Future<MemberDetailResult> getMemberDetail(num memberId) async {
    String? token = await TokenLib.getMemberToken();

    Dio dio = Dio();

    String _baseUrl = '$apiUri/member/detail/{memberId}';

    final response = await dio.get(
        _baseUrl.replaceAll('{memberId}', token!),
        options: Options(
            followRedirects: false,
            validateStatus: (state) {
              return state == 200;
            }));

    return MemberDetailResult.fromJson(response.data);
  }

  // 멤버 등록
  // Future<MemberJoinResult> setMember(MemberJoinRequest memberJoinRequest) async {
  //   Dio dio = Dio();
  //
  //   const String _baseUrl = '$apiUri/member/join';
  //
  //   final response = await dio.post(
  //       _baseUrl,
  //       data: memberJoinRequest.toJson(),
  //       options: Options(
  //           followRedirects: false,
  //           validateStatus: (status) {
  //             return status == 200;
  //           }));
  //
  //   return MemberJoinResult.toJson(response.data);
  // }

  // 로그인
  Future<LoginResult> doLogin(LoginRequest loginRequest) async {
    const String _baseUrl = 'http://travelmeeting.store:8080/v1/login/new'; // 주소 확인 해야함
    Dio dio = Dio();
    final response = await dio.post(
        _baseUrl,
      data: loginRequest.toJson(),
      options: Options(
        followRedirects: false,
        validateStatus: (status) {
          return status == 200;
        }
      )
    );
    return LoginResult.fromJson(response.data);
  }

  // // 정보 수정 put
  // Future<CommonResult> putMembers(num id) async {
  //   Dio dio = Dio();
  //
  //   String _baseUrl = '$apiUri/member/{id}';
  //
  //   final response =
  //   await dio.put(_baseUrl.replaceAll('{id}', id.toString()),
  //       options: Options(
  //           followRedirects: false,
  //           validateStatus: (state) {
  //             return state == 200;
  //           }));
  //
  //   return CommonResult.fromJson(response.data);
  // }
  //
  // // 회원 이름 수정
  // Future<CommonResult> putMemberName(num id) async {
  //   Dio dio = Dio();
  //
  //   String _baseUrl = '$apiUri/member-name/{id}';
  //
  //   final response =
  //   await dio.put(_baseUrl.replaceAll('{id}', id.toString()),
  //       options: Options(
  //           followRedirects: false,
  //           validateStatus: (state) {
  //             return state == 200;
  //           }));
  //
  //   return CommonResult.fromJson(response.data);
  // }
  //
  // // 회원 비밀번호 수정
  // Future<CommonResult> putMemberPw(num id) async {
  //   Dio dio = Dio();
  //
  //   String _baseUrl = '$apiUri/member-pw/{id}';
  //
  //   final response =
  //   await dio.put(_baseUrl.replaceAll('{id}', id.toString()),
  //       options: Options(
  //           followRedirects: false,
  //           validateStatus: (state) {
  //             return state == 200;
  //           }));
  //
  //   return CommonResult.fromJson(response.data);
  // }
  //
  // // 회원 전화 번호 수정
  // Future<CommonResult> putMemberPhone(num id) async {
  //   Dio dio = Dio();
  //
  //   String _baseUrl = '$apiUri/member-phone-number/{id}';
  //
  //   final response =
  //   await dio.put(_baseUrl.replaceAll('{id}', id.toString()),
  //       options: Options(
  //           followRedirects: false,
  //           validateStatus: (state) {
  //             return state == 200;
  //           }));
  //
  //   return CommonResult.fromJson(response.data);
  // }
  //
  // // 회원 이메일 수정
  // Future<CommonResult> putMemberEmail(num id) async {
  //   Dio dio = Dio();
  //
  //   String _baseUrl = '$apiUri/member-email/{id}';
  //
  //   final response =
  //   await dio.put(_baseUrl.replaceAll('{id}', id.toString()),
  //       options: Options(
  //           followRedirects: false,
  //           validateStatus: (state) {
  //             return state == 200;
  //           }));
  //
  //   return CommonResult.fromJson(response.data);
  // }
  //
  // // 회원 주소 수정
  // Future<CommonResult> putMemberAddress(num id) async {
  //   Dio dio = Dio();
  //
  //   String _baseUrl = '$apiUri/member-address/{id}';
  //
  //   final response =
  //   await dio.put(_baseUrl.replaceAll('{id}', id.toString()),
  //       options: Options(
  //           followRedirects: false,
  //           validateStatus: (state) {
  //             return state == 200;
  //           }));
  //
  //   return CommonResult.fromJson(response.data);
  // }
}