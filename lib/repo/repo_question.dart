// 1:1문의 레포


import 'package:dio/dio.dart';
import 'package:travel_meeting_app/config/config_api.dart';
import 'package:travel_meeting_app/model/question_detail_result.dart';
import 'package:travel_meeting_app/model/question_list_result.dart';

class RepoQuestion {
  // 게시글 전체 불러오기
  Future<QuestionListResult> getQuestions() async {
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/question/all';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return QuestionListResult.fromJson(response.data);
  }

  // 게시글 하나 불러 오기
  Future<QuestionDetailResult> getQuestion(num questionId) async {
    Dio dio = Dio();

    String _baseUrl = '$apiUri/question/detail/{questionId}'; // 주소 확인

    final response = await dio.get(
        _baseUrl.replaceAll('{questionId}', questionId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (state) {
              return state == 200;
            }
        )
    );

    return QuestionDetailResult.fromJson(response.data);
  }
}