// 댓글용 레포

import 'package:dio/dio.dart';
import 'package:travel_meeting_app/config/config_api.dart';
import 'package:travel_meeting_app/functions/token_lib.dart';
import 'package:travel_meeting_app/model/comments_list_result.dart';

class RepoComments {
  // 게시글 전체 불러오기
  Future<CommentsListResult> getComments() async {
    String? token = await TokenLib.getMemberToken();
    Dio dio = Dio();

    final String _baseUrl = '$apiUri/comments/all';

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommentsListResult.fromJson(response.data);
  }
}