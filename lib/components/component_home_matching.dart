// 홈용 매칭 컴포넌트

import 'package:flutter/material.dart';
import 'package:travel_meeting_app/config/config_path.dart';
import 'package:travel_meeting_app/config/config_size.dart';
import 'package:travel_meeting_app/model/matching_item.dart';

class ComponentHomeMatching extends StatelessWidget {
  const ComponentHomeMatching({super.key, required this.matchingItem, required this.callback});

  final MatchingItem matchingItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;

    return GestureDetector(
      onTap: callback,
      child: SingleChildScrollView(
        child: Row(
          children: [
            Column(
              children: [
                Image.asset(
                  pathBase + '${matchingItem.matchingImg}' ,
                  width: phoneWidth / 3.5,
                  height: phoneWidth / 5,
                  fit: BoxFit.fill,
                ),
                Text(
                    matchingItem.matchingName,
                  style: TextStyle(
                    fontSize: fontSizeMid,
                    fontWeight: fontWeightMid,
                  ),
                ), // 매칭 이름으로 바꿔야함
                Text('${matchingItem.matchingPrice}원', style: TextStyle(height: 1.2),), // 사이즈 조절 해야함
              ],
            ),
            SizedBox(width: 10,)
          ],
        ),
      ),
    );
  }
}
