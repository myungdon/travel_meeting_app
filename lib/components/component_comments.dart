// 댓글 복수용 컴포넌트

import 'package:flutter/material.dart';
import 'package:travel_meeting_app/components/main_divider.dart';
import 'package:travel_meeting_app/config/config_color.dart';
import 'package:travel_meeting_app/config/config_size.dart';
import 'package:travel_meeting_app/model/comments_item.dart';
import 'package:travel_meeting_app/model/question_item.dart';

class ComponentComments extends StatelessWidget {
  const ComponentComments({super.key, required this.callback, required this.commentsItem,});

  final CommentsItem commentsItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;
    double phoneHeight = MediaQuery.of(context).size.height;

    return GestureDetector(
      onTap: callback,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
                padding: EdgeInsets.all(6),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          '김남호',
                          // '${commentsItem.board}',
                          style: TextStyle(
                            fontSize: fontSizeMid,
                            fontWeight: fontWeightMid,
                          ),
                        ),
                        SizedBox(height: 5,),
                        Text(
                          '${commentsItem.comments}'
                        ),
                        SizedBox(height: 5,),
                        Row(
                          children: [
                            Text(
                                '${commentsItem.writingTime.substring(1, 16)}'
                            ),
                            TextButton(onPressed: () {}, child: Text('답글쓰기'))
                          ],
                        )
                      ],
                    ),
                  ],
                )
            ),
            MainDivider()
          ],
        ),
      ),
    );
  }
}