// 게시판 복수용 컴포넌트

import 'package:flutter/material.dart';
import 'package:travel_meeting_app/components/main_divider.dart';
import 'package:travel_meeting_app/config/config_color.dart';
import 'package:travel_meeting_app/config/config_size.dart';
import 'package:travel_meeting_app/model/board_item.dart';

class ComponentBoard extends StatelessWidget {
  const ComponentBoard({super.key, required this.boardItem, required this.callback,});

  final BoardItem boardItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;
    double phoneHeight = MediaQuery.of(context).size.height;

    // 날짜 변수
    String date = boardItem.dateWrite.substring(0, 10);
    String time = boardItem.dateWrite.substring(11, 16);
    String today = DateTime.now().toString().substring(0, 10);

    // 등록 날짜가 같으면 시간 표시 다르면 날짜 표시
    calculatePostTime() {
      if (date == today){
        return time;
      } else {
        return date;
      }
    }

    return GestureDetector(
      onTap: callback,
      child: SingleChildScrollView(
        child: Column(
          children: [
              Container(
                padding: EdgeInsets.all(6),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            boardItem.title,
                          style: TextStyle(
                            fontSize: fontSizeMid,
                            fontWeight: fontWeightMid,
                          ),
                        ),
                        SizedBox(height: 5,),
                        Row(
                          children: [
                            Text(
                                boardItem.memberName,
                              style: TextStyle(
                                fontSize: fontSizeMicro
                              ),
                            ),
                            SizedBox(width: 5,),
                            // Text(dateTime),
                            Text(
                              calculatePostTime(),
                              style: TextStyle(
                                fontSize: fontSizeMicro
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                    SizedBox(
                      child: Row(
                        children: [
                          // ClipRRect(
                          //   borderRadius: BorderRadius.circular(16),
                          //   child: Image.asset(
                          //       boardItem.img,
                          //     width: phoneWidth / 5,
                          //     height: phoneHeight / 11,
                          //     fit: BoxFit.cover,
                          //   ),
                          // ),
                          SizedBox(width: 4,),
                          ElevatedButton(
                              onPressed: () {},
                              style: ElevatedButton.styleFrom(
                                backgroundColor: colorThird,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(16)
                                ),
                                minimumSize: Size(0, 80),
                              ),
                              child: Column(
                                children: [
                                  Text('0', style: TextStyle(color: colorPrimary, fontSize: fontSizeMicro),),
                                  Text('댓글', style: TextStyle(color: colorPrimary, fontSize: fontSizeMicro),)
                                ],
                              )
                          )
                        ],
                      ),
                    ),
                  ],
                )
              ),
            MainDivider()
          ],
        ),
      ),
    );
  }
}
