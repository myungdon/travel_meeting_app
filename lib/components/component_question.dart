// 게시판 복수용 컴포넌트

import 'package:flutter/material.dart';
import 'package:travel_meeting_app/components/main_divider.dart';
import 'package:travel_meeting_app/config/config_color.dart';
import 'package:travel_meeting_app/config/config_size.dart';
import 'package:travel_meeting_app/model/question_item.dart';

class ComponentQuestion extends StatelessWidget {
  const ComponentQuestion({super.key, required this.questionItem, required this.callback,});

  final QuestionItem questionItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;
    double phoneHeight = MediaQuery.of(context).size.height;

    return GestureDetector(
      onTap: callback,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
                padding: EdgeInsets.all(6),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          questionItem.title,
                          style: TextStyle(
                            fontSize: fontSizeMid,
                            fontWeight: fontWeightMid,
                          ),
                        ),
                        SizedBox(height: 5,),
                        Row(
                          children: [
                            Text(
                              '${questionItem.memberName}',
                              style: TextStyle(
                                  fontSize: fontSizeMicro
                              ),
                            ),
                            SizedBox(width: 5,),
                            // Text(dateTime),
                            Text(
                              questionItem.inputDay,
                              style: TextStyle(
                                  fontSize: fontSizeMicro
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                    SizedBox(
                      child: Row(
                        children: [
                          ElevatedButton(
                              onPressed: () {},
                              style: ElevatedButton.styleFrom(
                                backgroundColor: colorThird,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(16)
                                ),
                                minimumSize: Size(0, 80),
                              ),
                              child: Column(
                                children: [
                                  Text('0', style: TextStyle(color: colorPrimary, fontSize: fontSizeMicro),),
                                  Text('댓글', style: TextStyle(color: colorPrimary, fontSize: fontSizeMicro),)
                                ],
                              )
                          )
                        ],
                      ),
                    ),
                  ],
                )
            ),
            MainDivider()
          ],
        ),
      ),
    );
  }
}