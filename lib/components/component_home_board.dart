// 홈용 게시판 컴포넌트

import 'package:flutter/material.dart';
import 'package:travel_meeting_app/config/config_path.dart';
import 'package:travel_meeting_app/config/config_size.dart';
import 'package:travel_meeting_app/model/board_item.dart';

class ComponentHomeBoard extends StatelessWidget {
  const ComponentHomeBoard({super.key, required this.boardItem, required this.callback});

  final BoardItem boardItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;

    return GestureDetector(
      onTap: callback,
      child: SingleChildScrollView(
        child: Row(
          children: [
            Column(
              children: [
                Image.asset(
                  pathBase + '${boardItem.img}' ,
                  width: phoneWidth / 4,
                  height: phoneWidth / 5,
                  fit: BoxFit.fill,
                ),
                Text(
                  boardItem.title,
                  style: TextStyle(
                    fontSize: fontSizeMid,
                    fontWeight: fontWeightMid,
                  ),
                ),
                Text(boardItem.memberName, style: TextStyle(height: 1.2),),
              ],
            ),
            SizedBox(width: 10,)
          ],
        ),
      ),
    );
  }
}
