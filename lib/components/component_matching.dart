// 매칭 전체용 컴포넌트

import 'package:flutter/material.dart';
import 'package:travel_meeting_app/components/main_divider.dart';
import 'package:travel_meeting_app/config/config_path.dart';
import 'package:travel_meeting_app/config/config_size.dart';
import 'package:travel_meeting_app/config/config_style.dart';
import 'package:travel_meeting_app/model/matching_item.dart';

class ComponentMatching extends StatelessWidget {
  const ComponentMatching({super.key, required this.matchingItem, required this.callback});

  final MatchingItem matchingItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    double phoneWidth = MediaQuery.of(context).size.width;

    return GestureDetector(
      onTap: callback,
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.all(common_sm_padding),
              child: Row(
                children: [
                  SizedBox(
                    child: Image.asset(
                      pathBase + '${matchingItem.matchingImg}',
                      width: phoneWidth / 2, // 이미지 넓이
                      height: phoneWidth / 3, //이미지 높이
                      fit: BoxFit.cover,
                    ),
                  ),
                  SizedBox(width: common_sm_padding,),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            matchingItem.matchingName,
                          style: const TextStyle(
                            fontSize: fontSizeMid,
                            fontWeight: fontWeightMid,
                          ),
                        ),
                        SizedBox(height: 2),
                        Text(
                          '${matchingItem.matchingPrice}원',
                          style: TextStyle(
                            fontSize: fontSizeSmall,
                            fontWeight: fontWeightSmall
                          ),),
                        SizedBox(height: 1),
                        Text('시작일 : ${matchingItem.dateStart}', style: TextStyle(fontSize: fontSizeMicro, fontWeight: fontWeightMicro),),
                        Text('종료일 : ${matchingItem.dateEnd}', style: TextStyle(fontSize: fontSizeMicro, fontWeight: fontWeightMicro),),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            SizedBox(
                              height: 15,
                              child: FittedBox(
                                child: Row(
                                  children: [
                                    Icon(Icons.chat_bubble_outline),
                                    Text('5'),
                                    Icon(Icons.favorite_outline),
                                    Text('10')
                                  ],
                                ),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            MainDivider()
          ],
        ),
    );
  }
}
