import 'package:flutter/material.dart';

class MainDivider extends StatelessWidget {
  const MainDivider({super.key});

  @override
  Widget build(BuildContext context) {
    return Divider(
      height: 1,
      color: Colors.grey[400],
      thickness: 1,
    );
  }
}
