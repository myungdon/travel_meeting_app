import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MemberLib {
  static void setMemberName(String memberName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberName', memberName);
  }

  static void setMemberAccount(String memberAccount) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberAccount', memberAccount);
  }

  static void setMemberPassword(String memberPassword) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberPassword', memberPassword);
  }

  static void setMemberPasswordRe(String memberPasswordRe) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberPasswordRe', memberPasswordRe);
  }

  static void setMemberBirthday(String memberBirthday) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberBirthday', memberBirthday);
  }

  static void setMemberPhoneNumber(String memberPhoneNumber) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberPhoneNumber', memberPhoneNumber);
  }

  static void setMemberIsMan(String memberIsMan) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberIsMan', memberIsMan);
  }

  static void setMemberEMail(String memberEMail) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberEMail', memberEMail);
  }

  static void setMemberAddress(String memberAddress) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberAddress', memberAddress);
  }
}