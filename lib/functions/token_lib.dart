import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:travel_meeting_app/pages/page_login.dart';

class TokenLib {
  static Future<String?> getMemberToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('memberToken');
  }
  static Future<String?> getMemberName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('memberName');
  }
  static void setMemberToken(String memberToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberToken', memberToken);
  }
  static void setMemberName(String memberName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberName', memberName);
  }

  static void logout(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.clear();

    Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (BuildContext context) => const PageLogin()),
          (route) => false
    );
  }
}